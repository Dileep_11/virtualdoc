package com.vd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication
public class VirtualDoctorApplication {

	public static void main(String[] args) {
		SpringApplication.run(VirtualDoctorApplication.class, args);
	}

}
