package com.vd.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.vd.dao.AdminDAO;
import com.vd.model.Admin;

@RestController
public class AdminController {
	@Autowired
	AdminDAO adminDAO;
	
	@GetMapping("/getAdminById/{adminId}")
	public Admin getAdminById(@PathVariable("adminId") int  adminId){
		Admin admin=adminDAO.getAdminById(adminId);
		if(admin!=null){
			return admin;
		}
		return new Admin(0, "Admin Not found", "");
	}
	
	@GetMapping("/getAdmin/{emailId}/{password}")
	public Admin getAdmin(@PathVariable("emailId") String emailID,@PathVariable("password") String password){
		return adminDAO.getAdmin(emailID,password);
	}
	
	

}
