package com.vd.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.vd.dao.AppointmentDAO;
import com.vd.model.Appointment;
import com.vd.repository.AppointmentRepository;

@RestController
public class AppointmentController {
	
	@Autowired
	AppointmentDAO appointmentDAO;
	

	@GetMapping("/getAllAppointments")
	public List<Appointment> getAllAppointments() {
		return appointmentDAO.getAllAppointments();
	}
	
	@GetMapping("/getAppointmentById/{appointmentID}")
	public Appointment getAppointmentById(@PathVariable("appointmentID") int appointmentId){
		Appointment appointment=appointmentDAO.getAppointmentById(appointmentId);
		if(appointment!=null){
			return appointment;
		}
		return new Appointment(0, null, null, "Appointment Not Found", null, null, null, null);
	}
	
	@PostMapping("/createAppointment")
	public String createAppointment(@RequestBody Appointment appointment){
		Appointment apt=appointmentDAO.createAppointment(appointment);
		System.out.println(apt);
		if(apt!=null){
			return "Appointment created Successfully";
		}
		return "Failed to create the Appointment";
	}
	
	@DeleteMapping("/deleteAppointment/{appontmentId}")
	public String deleteAppointment(@PathVariable("appointmentId") int appointmentId){
		appointmentDAO.deleteAppointment(appointmentId);
		return "Appointment("+appointmentId+") was deleted Successfully";
	}
	
	@PutMapping("/updateAppointment")
	public String updateAppointment(@RequestBody Appointment appointment){
		Appointment apt=appointmentDAO.createAppointment(appointment);
		if(apt!=null){
			return "Appointment Updated Successfully";
		}
		return "Failed to update the Appointment";
	}
	
	@GetMapping("/getAppointmentsByDocId/{docId}")
	public List<Appointment> getAppointmentsByDocId(@PathVariable("docId") int docId){
		return appointmentDAO.getAppointmentsByDocId(docId);
	}
	
	@GetMapping("/getAppointmentsByUserId/{userId}")
	public List<Appointment> getAppointmentsByUserId(@PathVariable("userId") int userId){
		return appointmentDAO.getAppointmentsByUserId(userId);
	}
	
	@GetMapping("/getLatestAppointmentsByUserId/{userId}")
	public List<Appointment> getLatestAppointmentsByUserId(@PathVariable("userId") int userId){
		return appointmentDAO.getAppointmentsByUserId(userId);
	}
	
	@GetMapping("/getPresIdByAppId/{appId}")
	public int getPresIdByAppId(@PathVariable("appId")int appId){
		Appointment app = appointmentDAO.getAppointmentById(appId);
		return app.getPrescription().getPresId();
	}
	
	

}
