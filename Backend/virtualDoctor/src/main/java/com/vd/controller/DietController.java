package com.vd.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.vd.dao.AppointmentDAO;
import com.vd.dao.DietDAO;
import com.vd.model.Appointment;
import com.vd.model.Diet;



@RestController
public class DietController {
	
	@Autowired  // Dependency Injection
	DietDAO dietDAO;
	
	@Autowired
	AppointmentDAO appointmentDAO;
	
	@PostMapping("/registerDiet")
	public Diet registerDiet(@RequestBody Diet diet) {
		Diet d=dietDAO.register(diet);
		if(d!=null) {
			Appointment app = appointmentDAO.getAppointmentById(d.getAppointment().getAppointmentID());
			app.setDiet(d);
			Appointment appres = appointmentDAO.createAppointment(app);
			return d;
		}
		return d;
	}
	
	@PutMapping("/updateDiet")
	public Diet updateDiet(@RequestBody Diet diet) {
		return dietDAO.updateDiet(diet);
	}
	
	@DeleteMapping("/deleteDiet/{dietID}")
	public String deleteDiet(@PathVariable("dietID") int dietID) {
		return dietDAO.deleteDiet(dietID);
	}
	
	
	@RequestMapping("/showAllDiets")
	public List<Diet> showAllDiets() {
		List<Diet> dietList = dietDAO.getAllDiets();
		return dietList;
	}
	
	@RequestMapping("/getDietById/{dietID}")
	public Diet getDietById(@PathVariable("dietID") int dietID) {
		
		Diet diet = dietDAO.getDiet(dietID);
		
		if (diet != null)
			return diet;
		
		return new Diet(0, "BreakFast Not Found!","Lunch Not Found","Dinner Not found", null, null, null);
	}
	
	@RequestMapping("/getDietByUser/{userId}")
	public List<Diet> getDietByUser(@PathVariable("userId") int userId) {
		
		List<Diet> diets = dietDAO.getDietByUser(userId);
		
		if (diets.get(0)!= null)
			return diets;
		
		return null;
	}
	
	@RequestMapping("/getLatestDietByUser/{userId}")
	public Diet getLatestDietByUser(@PathVariable("userId") int userId) {
		
		List<Diet> diet = dietDAO.getDietByUser(userId);
	
		if (diet.size()!=0)
			return diet.get(diet.size()-1);
		
		return new Diet(0, "BreakFast Not Found!","Lunch Not Found","Dinner Not found", null, null, null);
	}
}
