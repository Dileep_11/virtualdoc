package com.vd.controller;
import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.vd.model.Doctor;
import com.vd.model.User;
import com.vd.repository.DoctorRepository;
import com.vd.dao.DoctorDAO;

@RestController @CrossOrigin(origins = "http://localhost:4200")
public class DoctorController {
	
	

	@Autowired
	DoctorDAO doctorDAO;


	@GetMapping("/getAllDoctors")
	public List<Doctor> getAllDoctors() {
		return doctorDAO.getAllDoctors();
	}



	@GetMapping("/getDoctorById/{id}")
	public Doctor getDoctorById(@PathVariable("id")int doctorId ){


		Doctor doctor = doctorDAO.getDoctorById(doctorId);
		if(doctor!=null) {
			return doctor;
		}
		return null;

	}
	
	@GetMapping("/doctorValidate/{emailId}/{password}")
	public Doctor doctorValidate(@PathVariable("emailId") String emailId, @PathVariable("password") String password) {
		Doctor doctor = doctorDAO.doctorValidate(emailId, password);
		if(doctor!=null) {
			return doctor;
		}
		return null;
	}
	
	@GetMapping("/getDoctorIdByEmailId/{emailId}")
	public int getDoctorIdByEmailId(@PathVariable("emailId")String emailId) {
		Doctor doctor = doctorDAO.getDoctorIdByEmailId(emailId);
		if(doctor!=null) {
			return doctor.getDocId();
		}
		return 0;
	}

	@PostMapping("/registerDoctor")
	public String registerDoctor(@RequestBody Doctor doctor) {
		Doctor d = doctorDAO.register(doctor);
		if(d!=null) {
			return "Registered Successfully";
		}
		return "Registration failed";
	}


	@PutMapping("/updateDoctor")
	public String updateDoctor(@RequestBody Doctor doctor) {
		Doctor d = doctorDAO.register(doctor);
		if(d!=null) {
			return "Doctor details updated";
		}
		return "Updation failed";

	}

	@DeleteMapping("/deleteDoctor/{id}")
	public void deleteDoctor(@PathVariable int id) {
	doctorDAO.deleteDoctorById(id);
	}
	

}