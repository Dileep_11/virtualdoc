package com.vd.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.vd.dao.EmailDAO;
import com.vd.dao.PrescriptionDAO;
import com.vd.model.EmailDetails;
import com.vd.model.Prescription;

@RestController
public class EmailController {
	 @Autowired
	 private EmailDAO emailService;
	 
	 @Autowired
	 private PrescriptionDAO prescriptionService;
	 
	    // Sending a simple Email
	    @PostMapping("/sendMail")
	    public String sendMail(@RequestBody EmailDetails details)
	    {
	    	System.out.println(details);
//	    	for(MedRecords mr: details.getMedRecords()){
//	    		System.out.println(mr.getMedicine());
//	    		System.out.println(mr.getDosage());
//	    		System.out.println(mr.getInstructions());
//	    	}
	        String status= emailService.sendSimpleMail(details);
	        return status;
	    }
	    
	    @PostMapping("/sendMeetingId")
	    public String sendMeetingId(@RequestBody EmailDetails details){
	    	String status = emailService.sendMeetingID(details);
	    	return status;
	    }
	    
	 
	  
}
