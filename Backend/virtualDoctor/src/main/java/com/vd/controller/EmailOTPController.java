package com.vd.controller;

import com.vd.model.EmailOTP;
import com.vd.dao.EmailOTPDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EmailOTPController {
	 @Autowired 
	 private EmailOTPDAO emailService;
	 
	    // Sending a simple Email
	    @PostMapping("/sendOTPemail")
	    public String
	    sendMail(@RequestBody EmailOTP details)
	    {
	    	System.out.println(details);
	        String status
	            = emailService.sendSimpleMail(details);
	 
	        return status;
	    }
}
