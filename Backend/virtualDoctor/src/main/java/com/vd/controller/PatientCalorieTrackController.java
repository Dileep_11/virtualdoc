package com.vd.controller;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.vd.dao.PatientCalorieTrackDAO;
import com.vd.model.PatientCalorieTrack;

@RestController
public class PatientCalorieTrackController {
	@Autowired
	PatientCalorieTrackDAO pctDAO;
	
	
	@GetMapping("/getAllPCTracks")
	public List<PatientCalorieTrack> getAllPCTracks(){
		return pctDAO.getAllPatientCalorieTracks();
	}
	
	@GetMapping("/getPCTrackByID/{trackingId}")
	public PatientCalorieTrack getPCTrackById(@PathVariable("trackingId") int trackingId){
		PatientCalorieTrack pctrack =  pctDAO.getPCTrackById(trackingId);
		
		if(pctrack!=null){
			return pctrack;
		}
		else{
			return new PatientCalorieTrack();
		}
	
	}
	
	@PostMapping("/registerPCTrack")
	public PatientCalorieTrack registerPCTrack(@RequestBody PatientCalorieTrack pct){
		System.out.println(pct);
		PatientCalorieTrack pctrack = pctDAO.registerPCTrack(pct);
		if(pctrack!=null){
			return pctrack;
		}
		else{
			return new PatientCalorieTrack(0, 0, null, null);
		}
	}
	
	@PutMapping("/updatePCTrack")
	public PatientCalorieTrack updatePCTrack(@RequestBody PatientCalorieTrack pct) {
		
		PatientCalorieTrack pctrack = pctDAO.registerPCTrack(pct);
		
		if (pctrack != null)
			return pctrack;

		return new PatientCalorieTrack(0, 0, null, null);
	}
	
	@DeleteMapping("/deletePCTrack/{trackingId}")
	public String deletePCTrack(@PathVariable("trackingId") int trackingId) {
		pctDAO.deletePCTrack(trackingId);
		return "Patient Calorie Track with Id: (" + trackingId + ") Record Deleted Successfully!";
	}
	
	@GetMapping("/getPatientCTByUserId/{userId}")
	public List<PatientCalorieTrack> getPatientCTByUserId(@PathVariable("userId") int userId){
		return pctDAO.getPCTrackByUserId(userId);
	}
	
	
		
}
