package com.vd.controller;

	import java.util.List;

	import org.springframework.beans.factory.annotation.Autowired;
	import org.springframework.web.bind.annotation.DeleteMapping;
	import org.springframework.web.bind.annotation.GetMapping;
	import org.springframework.web.bind.annotation.PathVariable;
	import org.springframework.web.bind.annotation.PostMapping;
	import org.springframework.web.bind.annotation.PutMapping;
	import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.vd.dao.AppointmentDAO;
import com.vd.dao.PrescriptionDAO;
import com.vd.model.Appointment;
import com.vd.model.Diet;
import com.vd.model.Prescription;

@RestController
public class PrescriptionController {
		@Autowired
		AppointmentDAO appointmentDAO;

		@Autowired
		PrescriptionDAO prescriptionDAO;
		
		@GetMapping("/getAllPrescription")
		public List<Prescription> getAllPrescription(){
			return prescriptionDAO.getAllPrescription();
		}
		@GetMapping("/getPrescriptionById/{presId}")
		public Prescription getPrescriptionById(@PathVariable("presId")int presId) {
			Prescription prescription = prescriptionDAO.getPrescriptionById(presId);
			if(prescription!=null) {
				return prescription;
			}
			return null;
		}
//		@GetMapping("/getPrescriptionByappId/{appId}")
//		public Prescription getPrecriptionByappId(@PathVariable("appID")int appId) {
//			Prescription prescription = prescriptionDAO.getPrescriptionByappId(appId);
//			if(prescription!=null) {
//				return prescription;
//			}
//			return null;
//		}
		
		@PostMapping("/registerPrescription")
		public String addPrescription(@RequestBody Prescription prescription) {
			Prescription p = prescriptionDAO.addPrescription(prescription);
			if(p!=null) {
				Appointment app = appointmentDAO.getAppointmentById(p.getAppointment().getAppointmentID());
				app.setPrescription(p);
				//System.out.println(app);
				Appointment appres = appointmentDAO.createAppointment(app);
				//System.out.println(appres);
				return "Registered Successfully";
			}
			return "Registration failed";
		}
		
		
		@PutMapping("/updatePrescription")
		public String updatePrescription(@RequestBody Prescription prescription) {
			Prescription p = prescriptionDAO.addPrescription(prescription);
			if(p!=null) {
				return "Updated Successfully";
			}
			return "Updation failed";
		}
		
		
		@DeleteMapping("/deletePrescription/{presId}")
		public void deletePrescriptionById(@PathVariable("presId") int presId) {
			prescriptionDAO.deletePrescription(presId);
		}
		
		@RequestMapping("/getLatestPrescriptionByUser/{userId}")
		public Prescription getLatestPrescriptionByUser(@PathVariable("userId") int userId) {
			
			List<Appointment> appointments=appointmentDAO.getAppointmentsByUserId(userId);
			if(appointments.size()>0){
			int appId=appointments.get(appointments.size()-1).getAppointmentID();
			
			Prescription p= prescriptionDAO.getPrescriptionByAppId(appId);
			System.out.println(p+""+appId);
			
			if (p!=null)
				return p;
			}
			return new Prescription(0, "prescription not found");
		}
		
	}


