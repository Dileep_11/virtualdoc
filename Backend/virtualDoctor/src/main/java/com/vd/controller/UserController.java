package com.vd.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.vd.dao.UserDAO;
import com.vd.model.User;

@RestController
public class UserController {

	@Autowired
	UserDAO userDAO;
	
	@GetMapping("/getAllUsers")
	public List<User> getAllUsers(){
		return userDAO.getAllUsers();
	}
	@GetMapping("/getUserById/{userId}")
	public User getUserById(@PathVariable("userId")int userId) {
		User user = userDAO.getUserById(userId);
		if(user!=null) {
			return user;
		}
		return null;
	}
	
	@GetMapping("/getUserIdByEmailId/{emailId}")
	public int getUserIdByEmailId(@PathVariable("emailId")String emailId) {
		User user = userDAO.getUserIdByEmailId(emailId);
		if(user!=null) {
			return user.getUserId();
		}
		return 0;
	}
	
	@GetMapping("/userValidate/{emailId}/{password}")
	public User userValidate(@PathVariable("emailId") String emailId, @PathVariable("password") String password) {
		User user = userDAO.userValidate(emailId, password);
		if(user!=null) {
			return user;
		}
		return null;
	}
	@PostMapping("/registerUser")
	public String registerUser(@RequestBody User user) {
		User u = userDAO.register(user);
		if(u!=null) {
			return "Registered Successfully";
		}
		return "Registration failed";
	}
	@PutMapping("/updateUser")
	public String updateUser(@RequestBody User user) {
		User u = userDAO.register(user);
		if(u!=null) {
			return "Updated Successfully";
		}
		return "Updation failed";
	}
	@DeleteMapping("/deleteUserById/{userId}")
	public void deleteUserById(@PathVariable("userId") int userId) {
		userDAO.deleteUserById(userId);
	}
	
}
