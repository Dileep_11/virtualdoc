package com.vd.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vd.model.Admin;
import com.vd.repository.AdminRepository;
@Service
public class AdminDAO {
	
	@Autowired
	AdminRepository adminRepository;
	

	public Admin getAdminById(int adminId) {
		return adminRepository.findById(adminId).orElse(null);
	}


	public Admin getAdmin(String emailID, String password) {
		return adminRepository.findAdmin(emailID, password);
	}

}
