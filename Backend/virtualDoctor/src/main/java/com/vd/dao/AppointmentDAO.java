package com.vd.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vd.model.Appointment;
import com.vd.repository.AppointmentRepository;

@Service
public class AppointmentDAO {
	
	@Autowired
	AppointmentRepository appointmentRepository;
	
	public List<Appointment> getAllAppointments() {
		return appointmentRepository.findAll();
	}

	public Appointment getAppointmentById(int appointmentId) {
		return appointmentRepository.findById(appointmentId).orElse(null);
	}

	public Appointment createAppointment(Appointment appointment) {
		return appointmentRepository.save(appointment);
	}

	public void deleteAppointment(int appointmentId) {
		appointmentRepository.deleteById(appointmentId);
		
	}

	public List<Appointment> getAppointmentsByDocId(int docId) {

		return appointmentRepository.getAppointmentsByDocId(docId);
	}
	
	public List<Appointment> getAppointmentsByUserId(int userId) {

		return appointmentRepository.getAppointmentsByUserId(userId);
	}
	
}
