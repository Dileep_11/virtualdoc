package com.vd.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vd.model.Diet;
import com.vd.repository.DietRepository;
@Service
public class DietDAO {
	@Autowired
	DietRepository dietRepository;
	
	public Diet register(Diet diet) {
		return dietRepository.save(diet);
	}
	
	public Diet updateDiet(Diet diet) {
		return dietRepository.save(diet);
	}
	
	public String deleteDiet(int dietID) {
		System.out.println("Delete Diet: " + dietID);
		dietRepository.deleteById(dietID);
		return "Diet Deleted!";
	}
	
	public Diet getDiet(int dietID) {
		return dietRepository.findById(dietID).orElse(null);
	}
	
	public List<Diet> getAllDiets() {
		List<Diet> dietList = dietRepository.findAll();
		return dietList;
	}

	public List<Diet> getDietByUser(int userId) {
		return dietRepository.findDietByUser(userId);
	}

//	public Diet getLatestDietByUser(int userId) {
//		return dietRepository.findLatestDietByUser(userId);
//	}
	
	
}
