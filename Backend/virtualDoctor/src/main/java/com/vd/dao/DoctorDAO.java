package com.vd.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vd.model.Doctor;
import com.vd.model.User;
import com.vd.repository.DoctorRepository;

@Service
public class DoctorDAO {

	@Autowired
	private DoctorRepository doctorRepository;
	
	Doctor doctor= new Doctor();
	
	public List<Doctor> getAllDoctors(){
		return doctorRepository.findAll();
	}
//	Below service method will be used to register and update doctor
	public Doctor register(Doctor doctor) {
		return doctorRepository.save(doctor);
	}
	public Doctor getDoctorById(int doctorId){
		return doctorRepository.findById(doctorId).orElse(null);
	}
	public Doctor doctorValidate(String emailId, String password) {
		password = doctor.encryptPassword_doc(password);
		return doctorRepository.doctorValidate(emailId, password);
	}
	public void deleteDoctorById(int userId) {
		doctorRepository.deleteById(userId);
	}
	public Doctor getDoctorIdByEmailId(String emailId) {
		return doctorRepository.getIdByEmailId(emailId);
	}


}