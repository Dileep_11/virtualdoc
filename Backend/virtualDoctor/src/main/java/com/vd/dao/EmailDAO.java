package com.vd.dao;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.util.List;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.SimpleMailMessage;
import org.thymeleaf.spring4.SpringTemplateEngine;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import com.vd.model.EmailDetails;
import com.vd.model.MedRecords;
import com.vd.repository.EmailRepository;
@Service
public class EmailDAO implements EmailRepository{
	 
	@Autowired
    SpringTemplateEngine templateEngine;
	
	 @Autowired 
	 private JavaMailSender javaMailSender;
	 
	    @Value("${spring.mail.username}") private String sender;
	 
	    // Method 1
	    // To send a simple email
	    public String sendSimpleMail(EmailDetails details)
	    {
	 
	     
	        try {
	        	
	        	MimeMessage message = javaMailSender.createMimeMessage();
	            // Creating a simple mail message
	            MimeMessageHelper mailMessage
	                = new MimeMessageHelper(message,MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED,
	                        StandardCharsets.UTF_8.name());
	            List<MedRecords> medRecords = details.getMedRecords();
	            Context context = new Context();
	            context.setVariable("medRecords",medRecords);
	            context.setVariable("patientId", details.getPatientId());
	            context.setVariable("patientName", details.getPatientName());
	            context.setVariable("doctorId", details.getDoctorId());
	            context.setVariable("doctorName", details.getDoctorName());
	            context.setVariable("presId", details.getPresId());
				String html = templateEngine.process("email-template",context);
	            // Setting up necessary details
	            mailMessage.setFrom(sender);
	            mailMessage.setTo(details.getRecipient());
	            mailMessage.setText(html,true);
	            mailMessage.setSubject(details.getSubject());
	 
	            // Sending the mail
	            javaMailSender.send(message);
	            return "Mail Sent Successfully...";
	        }
	 
	        // Catch block to handle the exceptions
	        catch (Exception e) {
	             return   e.getMessage();
	        }
	    }
	    public String sendMeetingID(EmailDetails details)
	    {
	 
	     
	        try {
	        	
	        	MimeMessage message = javaMailSender.createMimeMessage();
	            // Creating a simple mail message
	            MimeMessageHelper mailMessage
	                = new MimeMessageHelper(message,MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED,
	                        StandardCharsets.UTF_8.name());
	            mailMessage.setFrom(sender);
	            mailMessage.setTo(details.getRecipient());
	            mailMessage.setText(details.getBody());
	            mailMessage.setSubject(details.getSubject());
	 
	            // Sending the mail
	            javaMailSender.send(message);
	            return "Mail Sent Successfully...";
	        }
	 
	        // Catch block to handle the exceptions
	        catch (Exception e) {
	             return   e.getMessage();
	        }
	    }

	 
	   
}
