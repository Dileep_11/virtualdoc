package com.vd.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vd.model.PatientCalorieTrack;
import com.vd.repository.PatientCalorieTrackRepository;

@Service
public class PatientCalorieTrackDAO{
	@Autowired
	PatientCalorieTrackRepository patientCalorieTrackRepository;

	public List<PatientCalorieTrack> getAllPatientCalorieTracks(){
		return patientCalorieTrackRepository.findAll();
	}
	public PatientCalorieTrack getPCTrackById(int trackingId){
		return patientCalorieTrackRepository.findById(trackingId).orElse(null);
	}
	public PatientCalorieTrack registerPCTrack(PatientCalorieTrack pctrack){
		return patientCalorieTrackRepository.save(pctrack);
	}
	public void deletePCTrack(int trackingId){
		patientCalorieTrackRepository.deleteById(trackingId);
	}
	public List<PatientCalorieTrack> getPCTrackByUserId(int userId) {
		return patientCalorieTrackRepository.findByUserId(userId);
	}
}
