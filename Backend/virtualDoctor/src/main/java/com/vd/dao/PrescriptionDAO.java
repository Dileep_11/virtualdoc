package com.vd.dao;



import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vd.model.Prescription;
import com.vd.repository.PrescriptionRepository;

@Service
public class PrescriptionDAO {

	@Autowired
	PrescriptionRepository prescriptionRepository;
	
	public List<Prescription> getAllPrescription(){
		return prescriptionRepository.findAll();
	}

	public Prescription addPrescription(Prescription prescription) {
		return prescriptionRepository.save(prescription);
	}

	
	public Prescription getPrescriptionById(int presId){
		return prescriptionRepository.findById(presId).orElse(null);
	}
	
	
	public void deletePrescription(int presId) {
		prescriptionRepository.deleteById(presId);
	}

	public Prescription getPrescriptionByAppId(int appId) {
		return prescriptionRepository.findPresByAppId(appId);
	}

//	public Prescription getPrescriptionByappId(int appId) {
//		
//		return prescriptionRepository.getPrescriptionByAppId(appId);
//	}
//	
	
}

