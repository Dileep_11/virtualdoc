package com.vd.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vd.model.User;
import com.vd.repository.UserRepository;

@Service
public class UserDAO {

	@Autowired
	UserRepository userRepository;
	
	User user= new User();
	
	public List<User> getAllUsers(){
		return userRepository.findAll();
	}
//	Below service method will be used to register and update user
	public User register(User user) {
		return userRepository.save(user);
	}
	public User getUserById(int userId){
		return userRepository.findById(userId).orElse(null);
	}
	public User userValidate(String emailId, String password) {
		password = user.encryptPassword(password);
		return userRepository.userValidate(emailId, password);
	}
	public void deleteUserById(int userId) {
		 userRepository.deleteById(userId);
	}
	public User getUserIdByEmailId(String emailId) {
		
		return userRepository.getIdByEmailId(emailId);
	}
	
	
}
