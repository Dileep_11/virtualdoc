package com.vd.model;

import java.sql.Date;
import java.sql.Time;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Appointment {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int appointmentID;
	private Time appFromTime;
	private Date appDate;
	private String status;
	

	@ManyToOne
	@JoinColumn(name="docId")
	private Doctor doctor;
	

	@ManyToOne
	@JoinColumn(name="userId")
	private User user;
	
	
	@OneToOne
	@JoinColumn(name="presId")
	private Prescription prescription;
	
	@OneToOne
	@JoinColumn(name="dietId")
	private Diet diet;

	
	public Appointment() {
		super();
		// TODO Auto-generated constructor stub
	}

	

	public Appointment(int appointmentID, Time appFromTime, Date appDate, String status, Doctor doctor, User user,
			Prescription prescription, Diet diet) {
		super();
		this.appointmentID = appointmentID;
		this.appFromTime = appFromTime;
		this.appDate = appDate;
		this.status = status;
		this.doctor = doctor;
		this.user = user;
		this.prescription = prescription;
		this.diet = diet;
	}

	

	@Override
	public String toString() {
		return "Appointment [appointmentID=" + appointmentID + ", appFromTime=" + appFromTime + ", appDate=" + appDate
				+ ", status=" + status + ", doctor=" + doctor + ", user=" + user + ", prescription=" + prescription
				+ ", diet=" + diet + "]";
	}



	public int getAppointmentID() {
		return appointmentID;
	}


	public void setAppointmentID(int appointmentID) {
		this.appointmentID = appointmentID;
	}


	public Time getAppFromTime() {
		return appFromTime;
	}


	public void setAppFromTime(Time appFromTime) {
		this.appFromTime = appFromTime;
	}


	public Date getAppDate() {
		return appDate;
	}


	public void setAppDate(Date appDate) {
		this.appDate = appDate;
	}


	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}


	public Doctor getDoctor() {
		return doctor;
	}


	public void setDoctor(Doctor doctor) {
		this.doctor = doctor;
	}


	public User getUser() {
		return user;
	}


	public void setUser(User user) {
		this.user = user;
	}


	public Prescription getPrescription() {
		return prescription;
	}


	public void setPrescription(Prescription prescription) {
		this.prescription = prescription;
	}


	public Diet getDiet() {
		return diet;
	}


	public void setDiet(Diet diet) {
		this.diet = diet;
	}


	

	

}
