package com.vd.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
public class Diet {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int dietID;
	private String breakFast;
	private String lunch;
	private String dinner;
	
	@ManyToOne
	@JoinColumn(name="userId")
	private User user;
	
	
	@ManyToOne
	@JoinColumn(name="docId")
	private Doctor doctor;
	
	@JsonIgnore
	@OneToOne
	@JoinColumn(name="appointmentID")
	private Appointment appointment;



	public Diet() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	



	public Diet(int dietID, String breakFast, String lunch, String dinner, User user, Doctor doctor,
			Appointment appointment) {
		super();
		this.dietID = dietID;
		this.breakFast = breakFast;
		this.lunch = lunch;
		this.dinner = dinner;
		this.user = user;
		this.doctor = doctor;
		this.appointment = appointment;
	}





	@Override
	public String toString() {
		return "Diet [dietID=" + dietID + ", breakFast=" + breakFast + ", lunch=" + lunch + ", dinner=" + dinner
				+ ", user=" + user + ", doctor=" + doctor + ", appointment=" + appointment + "]";
	}





	public int getDietID() {
		return dietID;
	}



	public void setDietID(int dietID) {
		this.dietID = dietID;
	}



	public String getBreakFast() {
		return breakFast;
	}



	public void setBreakFast(String breakFast) {
		this.breakFast = breakFast;
	}



	public String getLunch() {
		return lunch;
	}



	public void setLunch(String lunch) {
		this.lunch = lunch;
	}



	public String getDinner() {
		return dinner;
	}



	public void setDinner(String dinner) {
		this.dinner = dinner;
	}



	public User getUser() {
		return user;
	}



	public void setUser(User user) {
		this.user = user;
	}



	public Doctor getDoctor() {
		return doctor;
	}



	public void setDoctor(Doctor doctor) {
		this.doctor = doctor;
	}


	@JsonIgnore
	public Appointment getAppointment() {
		return appointment;
	}


	@JsonProperty
	public void setAppointment(Appointment appointment) {
		this.appointment = appointment;
	}




	


	
	
	
	

}
