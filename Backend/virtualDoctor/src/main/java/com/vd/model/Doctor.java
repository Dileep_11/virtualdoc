package com.vd.model;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;


import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity
public class Doctor {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int docId;
	private String name;
	private String password;
	private String education;
	private Date doj;
	private String speciality;
	private String gender;
	private double appointmentFee;
	
	@Column(unique=true)
	private String email;
	
	@JsonIgnore
	@OneToMany(mappedBy="doctor")
	List<Appointment> appointments = new ArrayList <Appointment>();
	
	
	@JsonIgnore
	@OneToMany(mappedBy="doctor")
	List<Diet> diet = new ArrayList <Diet>();
	
	final static String secretKey = "secrete";
	
	public Doctor() {
		super();
		// TODO Auto-generated constructor stub
	}



	public Doctor(int docId, String name, String password, String education, Date doj, String speciality, String gender,
			double appointmentFee, String email, List<Appointment> appointments) {
		super();
		this.docId = docId;
		this.name = name;
		this.password = password;
		this.education = education;
		this.doj = doj;
		this.speciality = speciality;
		this.gender = gender;
		this.appointmentFee = appointmentFee;
		this.email = email;
		this.appointments = appointments;
	}



	public int getDocId() {
		return docId;
	}



	public void setDocId(int docId) {
		this.docId = docId;
	}



	public String getName() {
		return name;
	}



	public void setName(String name) {
		this.name = name;
	}



	public String getPassword() {
		//System.out.println("Password is: "+this.password.length());
		if(this.password.length()>0){
			return (AESEncrDecr.decrypt(password,secretKey)).toString();
			}
			return "";
	}


	public void setPassword(String password) {
		if(password==""){
			this.password="";
		}
		if(password.length()>0){
		this.password = (AESEncrDecr.encrypt(password,secretKey)).toString();
		}
	}
	public static String encryptPassword_doc(String password) {
		password = (AESEncrDecr.encrypt(password,secretKey)).toString();
		return password;
	}


	public String getEducation() {
		if(this.education!=null){
			return education;
			}
		return "";
	}



	public void setEducation(String education) {
		this.education = education;
		if(this.education!=null){
			this.education=education;
		}
		this.education="";

	}



	public Date getDoj() {
		if(this.doj!=null){
			return doj;
			}
			return new Date(0000-00-00);
	}



	public void setDoj(Date doj) {
		this.doj = doj;
		if(this.doj!=null){
			this.doj=doj;
		}
		this.doj=new Date(0000-00-00);
	}



	public String getSpeciality() {
		
		if(this.speciality!=null){
			return speciality;
			}
		return "";
	}



	public void setSpeciality(String speciality) {
		this.speciality = speciality;
		if(this.speciality!=null){
			this.speciality=speciality;
		}
		this.speciality="";
	}



	public String getGender() {
		if(this.gender!=null){
			return gender;
			}
		return "";
	}



	public void setGender(String gender) {
		this.gender = gender;
		if(this.gender!=null){
			this.gender=gender;
		}
		this.gender="";
	}



	public double getappointmentFee() {
		return appointmentFee;
	}



	public void setappointmentFee(double appointmentFee) {
		this.appointmentFee = appointmentFee;
	}



	public String getEmail() {
		return email;
	}



	public void setEmail(String email) {
		this.email = email;
	}



	public List<Appointment> getAppointments() {
		return appointments;
	}



	public void setAppointments(List<Appointment> appointments) {
		this.appointments = appointments;
	}



	@Override
	public String toString() {
		return "Doctor [docId=" + docId + ", name=" + name + ", password=" + password + ", education=" + education
				+ ", doj=" + doj + ", speciality=" + speciality + ", gender=" + gender + ", appointmentFee="
				+ appointmentFee + ", email=" + email + ", appointments=" + appointments + "]";
	}
	
	
}


