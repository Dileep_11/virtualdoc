/**
 * 
 */
package com.vd.model;

import java.util.List;

/**
 * @author localadmin
 *
 */

public class EmailDetails {
	  	private String recipient;
	    private String subject;
	    private int patientId;
	    private String body;
	    private String patientName;
	    private int doctorId;
	    private String doctorName;
	    private int presId;
	    private List<MedRecords> medRecords;
		public EmailDetails() {
			super();
			// TODO Auto-generated constructor stub
		}
		public EmailDetails(String recipient, String subject, int patientId, String body, String patientName,
				int doctorId, String doctorName, int presId, List<MedRecords> medRecords) {
			super();
			this.recipient = recipient;
			this.subject = subject;
			this.patientId = patientId;
			this.body = body;
			this.patientName = patientName;
			this.doctorId = doctorId;
			this.doctorName = doctorName;
			this.presId = presId;
			this.medRecords = medRecords;
		}
		public String getRecipient() {
			return recipient;
		}
		public void setRecipient(String recipient) {
			this.recipient = recipient;
		}
		public String getSubject() {
			return subject;
		}
		public void setSubject(String subject) {
			this.subject = subject;
		}
		public int getPatientId() {
			return patientId;
		}
		public void setPatientId(int patientId) {
			this.patientId = patientId;
		}
		public String getBody() {
			return body;
		}
		public void setBody(String body) {
			this.body = body;
		}
		public String getPatientName() {
			return patientName;
		}
		public void setPatientName(String patientName) {
			this.patientName = patientName;
		}
		public int getDoctorId() {
			return doctorId;
		}
		public void setDoctorId(int doctorId) {
			this.doctorId = doctorId;
		}
		public String getDoctorName() {
			return doctorName;
		}
		public void setDoctorName(String doctorName) {
			this.doctorName = doctorName;
		}
		public int getPresId() {
			return presId;
		}
		public void setPresId(int presId) {
			this.presId = presId;
		}
		public List<MedRecords> getMedRecords() {
			return medRecords;
		}
		public void setMedRecords(List<MedRecords> medRecords) {
			this.medRecords = medRecords;
		}
		@Override
		public String toString() {
			return "EmailDetails [recipient=" + recipient + ", subject=" + subject + ", patientId=" + patientId
					+ ", body=" + body + ", patientName=" + patientName + ", doctorId=" + doctorId + ", doctorName="
					+ doctorName + ", presId=" + presId + ", medRecords=" + medRecords + "]";
		}
		
		
		 
}
