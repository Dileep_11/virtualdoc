package com.vd.model;

public class MedRecords{
	private String medicine;
	private String dosage;
	private String instructions;
	
	public MedRecords() {
		super();
		// TODO Auto-generated constructor stub
	}
	public MedRecords(String medicine, String dosage, String instructions) {
		super();
		this.medicine = medicine;
		this.dosage = dosage;
		this.instructions = instructions;
	}
	public String getMedicine() {
		return medicine;
	}
	public void setMedicine(String medicine) {
		this.medicine = medicine;
	}
	public String getDosage() {
		return dosage;
	}
	public void setDosage(String dosage) {
		this.dosage = dosage;
	}
	public String getInstructions() {
		return instructions;
	}
	public void setInstructions(String instructions) {
		this.instructions = instructions;
	}
	@Override
	public String toString() {
		return "MedRecords [medicine=" + medicine + ", dosage=" + dosage + ", instructions=" + instructions + "]";
	}
	
	
}
