package com.vd.model;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
public class PatientCalorieTrack {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int trackingId;
	private int calorieCount;
	private Date date;
	
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name="userId")
	private User user;
	

	public PatientCalorieTrack() {
		super();
		// TODO Auto-generated constructor stub
	}


	public PatientCalorieTrack(int trackingId, int calorieCount, Date date, User user) {
		super();
		this.trackingId = trackingId;
		this.calorieCount = calorieCount;
		this.date = date;
		this.user = user;
	}


	public int getTrackingId() {
		return trackingId;
	}


	public void setTrackingId(int trackingId) {
		this.trackingId = trackingId;
	}


	public int getCalorieCount() {
		return calorieCount;
	}


	public void setCalorieCount(int calorieCount) {
		this.calorieCount = calorieCount;
	}


	public Date getDate() {
		return date;
	}


	public void setDate(Date date) {
		this.date = date;
	}

	@JsonIgnore
	public User getUser() {
		return user;
	}

	@JsonProperty
	public void setUser(User user) {
		this.user = user;
	}


	@Override
	public String toString() {
		return "PaitientCalorieTrack [trackingId=" + trackingId + ", calorieCount=" + calorieCount + ", date=" + date
				+ ", user=" + user + "]";
	}
	
	
	

}
