package com.vd.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
public class Prescription {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int presId;
	private String content;
	
	@JsonIgnore
	@OneToOne
	@JoinColumn(name="appointmentID")
	private Appointment appointment;


	public Prescription() {
		super();
		// TODO Auto-generated constructor stub
	}


	public Prescription(int presId, String content) {
		super();
		this.presId = presId;
		this.content = content;
//		this.appointment = appointment;
	}


	public int getPresId() {
		return presId;
	}


	public void setPresId(int presId) {
		this.presId = presId;
	}


	public String getContent() {
		return content;
	}


	public void setContent(String content) {
		this.content = content;
	}

	@JsonIgnore
	public Appointment getAppointment() {
		return appointment;
	}

	@JsonProperty
	public void setAppointment(Appointment appointment) {
		this.appointment = appointment;
	}


	@Override
	public String toString() {
		return "Prescription [presId=" + presId + ", content=" + content+ "]";
	}

	
	
	
	
	

}
