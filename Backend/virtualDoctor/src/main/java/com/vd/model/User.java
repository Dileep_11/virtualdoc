package com.vd.model;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class User {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int userId;
	@Column(unique=true)
	private String emailId;
	private String name;
	private String password;
	private Date dob;
	private String gender;
	private String mobile;
	
	@JsonIgnore
	@OneToMany(mappedBy="user")
	List<Appointment> appointments = new ArrayList <Appointment>();
	
	@JsonIgnore
	@OneToMany(mappedBy="user")
	List<PatientCalorieTrack> paitientCalorieTrack = new ArrayList <PatientCalorieTrack>();
	
	@JsonIgnore
	@OneToMany(mappedBy="user")
	List<Diet> diet = new ArrayList <Diet>();
	
	final static String secretKey = "secrete";
	
	public User() {
		super();
		// TODO Auto-generated constructor stub
	}

	public User(int userId, String emailId, String name, String password, Date dob, String gender, String mobile,
			List<Appointment> appointments, List<PatientCalorieTrack> paitientCalorieTrack) {
		super();
		this.userId = userId;
		this.emailId = emailId;
		this.name = name;
		this.password = password;
		this.dob = dob;
		this.gender = gender;
		this.mobile = mobile;
		this.appointments = appointments;
		this.paitientCalorieTrack = paitientCalorieTrack;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		//System.out.println("Password is: "+this.password.length());
		if(this.password.length()>0){
		return (AESEncrDecr.decrypt(password,secretKey)).toString();
		}
		return "";
	}
	public void setPassword(String password) {
		if(password==""){
			this.password="";
		}
		if(password.length()>0){
		this.password = (AESEncrDecr.encrypt(password,secretKey)).toString();
		}
		//this.password = password;
	}
	
	public static String encryptPassword(String password) {
		password = (AESEncrDecr.encrypt(password,secretKey)).toString();
		//this.password = password;
		return password;
	}

	public Date getDob() {
		if(this.dob!=null){
		return dob;
		}
		return new Date(0000-00-00);
	}

	public void setDob(Date dob) {
		this.dob = dob;
		if(this.dob!=null){
			this.dob=dob;
		}
		this.dob=new Date(0000-00-00);
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public List<Appointment> getAppointments() {
		return appointments;
	}

	public void setAppointments(List<Appointment> appointments) {
		this.appointments = appointments;
	}

	public List<PatientCalorieTrack> getPaitientCalorieTrack() {
		return paitientCalorieTrack;
	}

	public void setPaitientCalorieTrack(List<PatientCalorieTrack> paitientCalorieTrack) {
		this.paitientCalorieTrack = paitientCalorieTrack;
	}

	@Override
	public String toString() {
		return "User [userId=" + userId + ", emailId=" + emailId + ", name=" + name + ", password=" + password
				+ ", dob=" + dob + ", gender=" + gender + ", mobile=" + mobile + ", appointments=" + appointments
				+ ", paitientCalorieTrack=" + paitientCalorieTrack + "]";
	}
	
	

}
class AESEncrDecr {
    private static SecretKeySpec secretKey;
    private static byte[] key;
    private static final String ALGORITHM = "AES";

    public static void prepareSecreteKey(String myKey) {
        MessageDigest sha = null;
        try {
            key = myKey.getBytes(StandardCharsets.UTF_8);
            sha = MessageDigest.getInstance("SHA-1");
            key = sha.digest(key);
            key = Arrays.copyOf(key, 16);
            secretKey = new SecretKeySpec(key, ALGORITHM);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    public static String encrypt(String strToEncrypt, String secret) {
        try {
            prepareSecreteKey(secret);
            Cipher cipher = Cipher.getInstance(ALGORITHM);
            cipher.init(Cipher.ENCRYPT_MODE, secretKey);
            return Base64.getEncoder().encodeToString(cipher.doFinal(strToEncrypt.getBytes("UTF-8")));
        } catch (Exception e) {
            System.out.println("Error while encrypting: " + e.toString());
        }
        return null;
    }

    public static String decrypt(String strToDecrypt, String secret) {
    	if(strToDecrypt.length()!=0){
        try {
            prepareSecreteKey(secret);
            Cipher cipher = Cipher.getInstance(ALGORITHM);
            cipher.init(Cipher.DECRYPT_MODE, secretKey);
            return new String(cipher.doFinal(Base64.getDecoder().decode(strToDecrypt)));
        } catch (Exception e) {
            System.out.println("Error while decrypting: " + e.toString());
        }
    	}
        return null;
    }
    	
   
}
