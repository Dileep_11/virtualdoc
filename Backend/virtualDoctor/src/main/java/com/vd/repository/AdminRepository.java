package com.vd.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.vd.model.Admin;
@Repository
public interface AdminRepository extends JpaRepository<Admin, Integer> {

	@Query("from Admin a where a.emailId = :emailId and a.password = :password")
	public Admin findAdmin(@Param("emailId") String emailId, @Param("password") String password);

}
