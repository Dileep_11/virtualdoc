package com.vd.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.vd.model.Appointment;

@Repository
public interface AppointmentRepository extends JpaRepository<Appointment, Integer> {
	@Query("from Appointment a where a.doctor.docId= :docId")
	List<Appointment> getAppointmentsByDocId(@Param("docId") int docId);
	
	@Query("from Appointment a where a.user.userId= :userId ORDER BY a.user.userId")
	List<Appointment> getAppointmentsByUserId(@Param("userId") int userId);

}
