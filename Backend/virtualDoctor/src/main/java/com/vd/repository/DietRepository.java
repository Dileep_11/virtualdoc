package com.vd.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.vd.model.Diet;
@Repository
public interface DietRepository extends JpaRepository<Diet, Integer> {
	
	@Query(value = "from Diet d where d.user.userId = :userId ORDER BY d.dietID")
	List<Diet> findDietByUser(@Param("userId") int userId);

	

	

}
