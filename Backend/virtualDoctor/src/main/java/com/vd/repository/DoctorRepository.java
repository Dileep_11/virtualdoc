package com.vd.repository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.vd.model.Doctor;

@Repository
public interface DoctorRepository extends JpaRepository<Doctor, Integer>{
	@Query("from Doctor d where d.email = :emailId and d.password = :password")
	Doctor doctorValidate(@Param("emailId" )String emailId,@Param("password" ) String password);

	@Query("from Doctor d where d.email = :emailId" )
	Doctor getIdByEmailId(@Param("emailId" )String emailId);

}
