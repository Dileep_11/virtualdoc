package com.vd.repository;

import com.vd.model.EmailOTP;

public interface EmailOTPRepository {
	
	String sendSimpleMail(EmailOTP details);
}
