package com.vd.repository;

import com.vd.model.EmailDetails;

public interface EmailRepository {

    // Method
    // To send a simple email
    String sendSimpleMail(EmailDetails details);
 
}
