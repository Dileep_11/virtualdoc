package com.vd.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.vd.model.PatientCalorieTrack;
@Repository
public interface PatientCalorieTrackRepository extends JpaRepository<PatientCalorieTrack,Integer>{

	@Query("from PatientCalorieTrack p where p.user.userId= :userId")
	List<PatientCalorieTrack> findByUserId(@Param("userId")int userId);
}
