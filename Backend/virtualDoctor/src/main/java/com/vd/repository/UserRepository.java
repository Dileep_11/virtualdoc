package com.vd.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.vd.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, Integer>{
	@Query("from User u where u.emailId = :emailId and u.password = :password")
	User userValidate(@Param("emailId")String emailId, @Param("password")String password);

	@Query("from User u where u.emailId = :emailId ")
	User getIdByEmailId(@Param("emailId")String emailId);
	
}
