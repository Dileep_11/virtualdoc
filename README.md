Virtual Doc

Entities : 
User                            (mailID(PK) , password, age, gender, mobile)
Doctor                    (DocId(Pk), password, education, experience, speciality, gender, appoitmentFee, mobile)
Admin                          (AdminId(PK) , password)
Appointment                (AppointmentID(PK), appFromTime, appDate, docId(FK)(M-O), userId(FK)(M-O), status)
Prescription                 (presId(PK), content, appId(FK)(O-O)).
Diet                             (dietID(PK), userID(Fk)(M-O), docId(Fk)(M-O), breakFast, Lunch, Dinner)
PaitientCalorieTrack   (trackingId(PK), UserId(FK)(M-O), calorieCount, date)



Functions
User: 
Login and logout
register
Request an appointment/cancel
Attend the appointment
Check prescription
Check food nutrition.
Check bmi.
Check Calorie(add meals of the day)

Doctor:
Login and logout
Register
Check Appointments
Accept appointments -> getUserDetails(name, calorie)
Give prescription


Admin: 
Login and logout
Update/delete user
Update/delete Doctor

