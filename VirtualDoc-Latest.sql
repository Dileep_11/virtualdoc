-- MySQL dump 10.13  Distrib 8.0.31, for Win64 (x86_64)
--
-- Host: localhost    Database: virtualdoc
-- ------------------------------------------------------
-- Server version	8.0.31

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admin`
--

DROP TABLE IF EXISTS `admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `admin` (
  `admin_id` int NOT NULL AUTO_INCREMENT,
  `email_id` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`admin_id`),
  UNIQUE KEY `UK_72auwcxdo976d396pv0f1xc1w` (`email_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin`
--

LOCK TABLES `admin` WRITE;
/*!40000 ALTER TABLE `admin` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `appointment`
--

DROP TABLE IF EXISTS `appointment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `appointment` (
  `appointmentid` int NOT NULL AUTO_INCREMENT,
  `app_date` date DEFAULT NULL,
  `app_from_time` time DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `diet_id` int DEFAULT NULL,
  `doc_id` int DEFAULT NULL,
  `pres_id` int DEFAULT NULL,
  `user_id` int DEFAULT NULL,
  PRIMARY KEY (`appointmentid`),
  KEY `FKm5dooiahli2732tuud5au7m69` (`diet_id`),
  KEY `FK3ox9pj7opi2sh7hr5ceim6v8h` (`doc_id`),
  KEY `FKlcv1p034hq5y9vuypb1cry95g` (`pres_id`),
  KEY `FKa8m1smlfsc8kkjn2t6wpdmysk` (`user_id`),
  CONSTRAINT `FK3ox9pj7opi2sh7hr5ceim6v8h` FOREIGN KEY (`doc_id`) REFERENCES `doctor` (`doc_id`),
  CONSTRAINT `FKa8m1smlfsc8kkjn2t6wpdmysk` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`),
  CONSTRAINT `FKlcv1p034hq5y9vuypb1cry95g` FOREIGN KEY (`pres_id`) REFERENCES `prescription` (`pres_id`),
  CONSTRAINT `FKm5dooiahli2732tuud5au7m69` FOREIGN KEY (`diet_id`) REFERENCES `diet` (`dietid`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `appointment`
--

LOCK TABLES `appointment` WRITE;
/*!40000 ALTER TABLE `appointment` DISABLE KEYS */;
INSERT INTO `appointment` VALUES (1,'2023-02-15','14:30:00','completed',1,8,1,1),(2,'2023-02-15','12:41:00','completed',2,8,2,1),(3,'2023-02-15','19:58:00','completed',3,8,3,1),(4,'2023-02-15','10:32:00','completed',NULL,7,NULL,1),(5,'2023-02-15','10:36:00','completed',5,8,5,1),(6,'2023-02-15','15:17:00','completed',6,8,6,1);
/*!40000 ALTER TABLE `appointment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `diet`
--

DROP TABLE IF EXISTS `diet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `diet` (
  `dietid` int NOT NULL AUTO_INCREMENT,
  `break_fast` varchar(255) DEFAULT NULL,
  `dinner` varchar(255) DEFAULT NULL,
  `lunch` varchar(255) DEFAULT NULL,
  `appointmentid` int DEFAULT NULL,
  `doc_id` int DEFAULT NULL,
  `user_id` int DEFAULT NULL,
  PRIMARY KEY (`dietid`),
  KEY `FK3pdxj97xu9nf4u9i3yxpe76j2` (`appointmentid`),
  KEY `FKo7va66cl39tu7cnwukm8l688i` (`doc_id`),
  KEY `FKro8td081sdv1r1btel5d3lkw5` (`user_id`),
  CONSTRAINT `FK3pdxj97xu9nf4u9i3yxpe76j2` FOREIGN KEY (`appointmentid`) REFERENCES `appointment` (`appointmentid`),
  CONSTRAINT `FKo7va66cl39tu7cnwukm8l688i` FOREIGN KEY (`doc_id`) REFERENCES `doctor` (`doc_id`),
  CONSTRAINT `FKro8td081sdv1r1btel5d3lkw5` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `diet`
--

LOCK TABLES `diet` WRITE;
/*!40000 ALTER TABLE `diet` DISABLE KEYS */;
INSERT INTO `diet` VALUES (1,'Fruit Salad','Rice and Laice','Veg Curry',1,8,1),(2,'Fruit Salad','Fish curry','Veg Soup',2,8,1),(3,'Fruit salad and eggs','Rice and latice','Chapathi with chicken',3,8,1),(4,'Fruit','Soup','Eggs',4,7,1),(5,'Veg Soup','Rice and Dal','Eggs',5,8,1),(6,'Veg Soup','Rice and Dal','Fruit Salad',6,8,1);
/*!40000 ALTER TABLE `diet` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `doctor`
--

DROP TABLE IF EXISTS `doctor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `doctor` (
  `doc_id` int NOT NULL AUTO_INCREMENT,
  `appointment_fee` double NOT NULL,
  `doj` date DEFAULT NULL,
  `education` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `speciality` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`doc_id`),
  UNIQUE KEY `UK_jdtgexk368pq6d2yb3neec59d` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `doctor`
--

LOCK TABLES `doctor` WRITE;
/*!40000 ALTER TABLE `doctor` DISABLE KEYS */;
INSERT INTO `doctor` VALUES (1,1200,'2000-09-09','MBBS','reddy@gmail.com','male','Reddy','5DKL+JCI5MZe4g7CCGmDtA==','Diabetes'),(2,1600,'2001-09-09','MD','naidu@gmail.com','male','Naidu','5DKL+JCI5MZe4g7CCGmDtA==','Surgeon'),(3,400,'2002-09-09','B Pharm','sharma@gmail.com','male','Sharma','5DKL+JCI5MZe4g7CCGmDtA==','Heart Specialist'),(4,800,'2003-09-09','M Pharm','patil@gmail.com','male','Patil','5DKL+JCI5MZe4g7CCGmDtA==','Audiologists'),(5,1200,'2002-04-09','MBBS','lad@gmail.com','male','Lad','5DKL+JCI5MZe4g7CCGmDtA==','Allergist '),(6,300,'2006-09-09','BHMS','verma@gmail.com','male','Verma','5DKL+JCI5MZe4g7CCGmDtA==','Dermatologist'),(7,1200,'2002-05-09','MBBS','mehta@gmail.com','female','Mehta','5DKL+JCI5MZe4g7CCGmDtA==','Allergist '),(8,1200,'2002-01-09','MD','modi@gmail.com','female','Modi','5DKL+JCI5MZe4g7CCGmDtA==','Allergist ');
/*!40000 ALTER TABLE `doctor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `patient_calorie_track`
--

DROP TABLE IF EXISTS `patient_calorie_track`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `patient_calorie_track` (
  `tracking_id` int NOT NULL AUTO_INCREMENT,
  `calorie_count` int NOT NULL,
  `date` date DEFAULT NULL,
  `user_id` int DEFAULT NULL,
  PRIMARY KEY (`tracking_id`),
  KEY `FKeftsd4q08c36lu1tafxu9xjn2` (`user_id`),
  CONSTRAINT `FKeftsd4q08c36lu1tafxu9xjn2` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `patient_calorie_track`
--

LOCK TABLES `patient_calorie_track` WRITE;
/*!40000 ALTER TABLE `patient_calorie_track` DISABLE KEYS */;
INSERT INTO `patient_calorie_track` VALUES (1,504,'2023-02-15',1),(2,460,'2023-02-15',1),(3,504,'2023-02-15',1),(4,460,'2023-02-15',1);
/*!40000 ALTER TABLE `patient_calorie_track` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prescription`
--

DROP TABLE IF EXISTS `prescription`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `prescription` (
  `pres_id` int NOT NULL AUTO_INCREMENT,
  `content` varchar(255) DEFAULT NULL,
  `appointmentid` int DEFAULT NULL,
  PRIMARY KEY (`pres_id`),
  KEY `FK10wilmnseyannvpmfj0kibyxp` (`appointmentid`),
  CONSTRAINT `FK10wilmnseyannvpmfj0kibyxp` FOREIGN KEY (`appointmentid`) REFERENCES `appointment` (`appointmentid`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prescription`
--

LOCK TABLES `prescription` WRITE;
/*!40000 ALTER TABLE `prescription` DISABLE KEYS */;
INSERT INTO `prescription` VALUES (1,'paracetamol,15,afternoon - after food||Dolo 650,12,afternoon - after food||',1),(2,'paracetamol,500mg,afternoon - after food||Dolo 650,15,afternoon - after food||',2),(3,'Dolo 650,12,afternoon - after food||paracetamol,7,Night - after food||',3),(4,'paracetamol,500mg,afternoon - after food||Dolo 650,12,afternoon - after food||',4),(5,'Dolo 650,500mg,afternoon - after food||paracetamol,12,afternoon - after food||',5),(6,'Dolo 650,1,afternoon - after food||paracetamol,1,afternoon - after food||',6);
/*!40000 ALTER TABLE `prescription` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user` (
  `user_id` int NOT NULL AUTO_INCREMENT,
  `dob` date DEFAULT NULL,
  `email_id` varchar(255) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `mobile` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `UK_r9kvst217faqa7vgeyy51oos0` (`email_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'1970-01-01','mohdirfandarood786@gmail.com','male','9182653010','Hussain','ALmMazI6FQz9CQCYGkbmDw=='),(2,'1970-01-01','james@gmail.com','male','9182653010','james','3NaspW6i/pZOh7aObaiYCg=='),(3,'1970-01-01','john@gmail.com','male','9182653010','Jack','i892elDdoNYchwNXJYASpA=='),(4,'1970-01-01','jack@gmail.com','male','9182653010','Jack','ygTEmJACm1FGauTQJfC0+Q=='),(6,'1970-01-01','rock@gmail.com','male','9182653010','rock','zwYJgOpYxkElSGAdHa2vVg=='),(7,'1970-01-01','rock2@gmail.com','male','9052353396','Jack','zwYJgOpYxkElSGAdHa2vVg=='),(8,'1970-01-01','jony@gmail.com','male','9052353396','jony','zwYJgOpYxkElSGAdHa2vVg==');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-02-15 12:41:09
