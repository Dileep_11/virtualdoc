export class PhoneNumber {
    number: any
  
    // format phone numbers as E.164
    get e164() {
      const num = this.number
      return `+${num}`
    }
  
  }