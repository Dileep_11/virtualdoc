import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import * as path from 'path';
import { AuthGuard } from './auth.guard';
import { AppointmentsComponent } from './appointments/appointments.component';
import { BioComponent } from './bio/bio.component';
import { CalorieTrackerComponent } from './calorie-tracker/calorie-tracker.component';
import { DietForUserComponent } from './diet-for-user/diet-for-user.component';

import { DoctorAuthGuard } from './doctor-auth.guard';
import { DoctorDashboardComponent } from './doctor-dashboard/doctor-dashboard.component';
import { DoctorListComponent } from './doctor-list/doctor-list.component';
import { DoctorLoginComponent } from './doctor-login/doctor-login.component';
import { ForgotPasswordDoctorComponent } from './forgot-password-doctor/forgot-password-doctor.component';
import { ForgotPasswordUserComponent } from './forgot-password-user/forgot-password-user.component';
import { LoginComponent } from './login/login.component';
import { LogoutComponent } from './logout/logout.component';
import { PrescriptionComponent } from './prescription/prescription.component';
import { RegisterComponent } from './register/register.component';
import { ResetPasswordDoctorComponent } from './reset-password-doctor/reset-password-doctor.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { UserDashboardComponent } from './user-dashboard/user-dashboard.component';
import { UserRegisterComponent } from './user-register/user-register.component';
import { DoctorAppointmentsComponent } from './doctor-appointments/doctor-appointments.component';
import { VideoCallComponent } from './video-call/video-call.component';
import { VideoCall2Component } from './video-call2/video-call2.component';
import { ShowPrescriptionComponent } from './show-prescription/show-prescription.component';
import { DietComponent } from './diet/diet.component';
import { ShowDietComponent } from './show-diet/show-diet.component';


const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: '', component: BioComponent },
  { path: 'doctorlogin', component: DoctorLoginComponent },
  { path: 'register', component: RegisterComponent },
  {
    path: 'doctorDashboard',
    canActivate: [DoctorAuthGuard],
    component: DoctorDashboardComponent,
  },
  {
    path: 'userDashboard',
    canActivate: [AuthGuard],
    component: UserDashboardComponent,
  },
  {
    path: 'calorieTracker-user',
    canActivate: [AuthGuard],
    component: CalorieTrackerComponent,
  },
  {
    path: 'calorieTracker-doctor',
    canActivate: [DoctorAuthGuard],
    component: CalorieTrackerComponent,
  },
  {
    path: 'dietForUser',
    canActivate: [AuthGuard],
    component: DietForUserComponent,
  },
  { path: 'logout', component: LogoutComponent },
  { path: 'userRegister', component: UserRegisterComponent },
  {
    path: 'doctorlist',
    canActivate: [AuthGuard],
    component: DoctorListComponent,
  },
  { path: 'ForgotPasswordUser', component: ForgotPasswordUserComponent },
  { path: 'ResetpasswordUser', component: ResetPasswordComponent },
  { path: 'ForgotPasswordDoctor', component: ForgotPasswordDoctorComponent },
  { path: 'ResetpasswordDoctor', component: ResetPasswordDoctorComponent },

  // {path:"doctorlogin",component:DoctorLoginComponent},
  // {path:"register",component:RegisterComponent},
  // {path:"doctorDashboard",canActivate:[DoctorAuthGuard],component:DoctorDashboardComponent},
  // {path:"userDashboard",canActivate:[AuthGuard],component:UserDashboardComponent},
  {path:"calorieTracker-user",canActivate:[AuthGuard],component:CalorieTrackerComponent},
  {path:"calorieTracker-doctor",canActivate:[DoctorAuthGuard],component:CalorieTrackerComponent},
  {path:'dietForUser',canActivate:[AuthGuard],component:DietForUserComponent},
  // {path:'logout',component:LogoutComponent},
  // {path:'userRegister',component:UserRegisterComponent},
  // {path:'doctorlist',canActivate:[AuthGuard], component: DoctorListComponent },
  // {path:'ForgotPasswordUser',component: ForgotPasswordUserComponent},
  // {path:'ResetpasswordUser',component:ResetPasswordComponent},
  // {path:'ForgotPasswordDoctor',component:ForgotPasswordDoctorComponent},
  // {path:'ResetpasswordDoctor',component:ResetPasswordDoctorComponent},
  {path:'doctorAppointments',canActivate:[DoctorAuthGuard],component:DoctorAppointmentsComponent},
  { path: 'appointments',canActivate:[AuthGuard], component: AppointmentsComponent },
  { path: 'showPrescription',canActivate:[AuthGuard], component: ShowPrescriptionComponent },
  {path:'dietForUser',canActivate:[AuthGuard],component:DietForUserComponent},
  {path:'logout',component:LogoutComponent},
  {path:'userRegister',component:UserRegisterComponent},
  {path:'doctorlist',canActivate:[AuthGuard], component: DoctorListComponent },
  {path:"prescription", component:PrescriptionComponent},
  {path:"diet", component:DietComponent},
  {path:"showDiet", component:ShowDietComponent},
  {path:"video",component:VideoCallComponent},
  {path:"video2",component:VideoCall2Component}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule],
})
export class AppRoutingModule {}
