import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { LogoutComponent } from './logout/logout.component';
import { DoctorDashboardComponent } from './doctor-dashboard/doctor-dashboard.component';
import {
  SocialLoginModule,
  SocialAuthServiceConfig,
} from '@abacritt/angularx-social-login';
import { GoogleLoginProvider } from '@abacritt/angularx-social-login';
import { UserDashboardComponent } from './user-dashboard/user-dashboard.component';
import { DoctorLoginComponent } from './doctor-login/doctor-login.component';
import { CalorieTrackerComponent } from './calorie-tracker/calorie-tracker.component';
import { DietForUserComponent } from './diet-for-user/diet-for-user.component';
import { DoctorListComponent } from './doctor-list/doctor-list.component';
import { ExpPipe } from './exp.pipe';
import { PrescriptionComponent } from './prescription/prescription.component';
import { DietComponent } from './diet/diet.component';
import { OverlayModule } from '@angular/cdk/overlay';

import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { UserRegisterComponent } from './user-register/user-register.component';
import { ForgotPasswordUserComponent } from './forgot-password-user/forgot-password-user.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { ForgotPasswordDoctorComponent } from './forgot-password-doctor/forgot-password-doctor.component';
import { ResetPasswordDoctorComponent } from './reset-password-doctor/reset-password-doctor.component';
import { CommonModule, DatePipe } from '@angular/common';
import { AppointmentServiceService } from './appointment-service.service';
import { AppointmentsComponent } from './appointments/appointments.component';
import { DoctorAppointmentsComponent } from './doctor-appointments/doctor-appointments.component';
import { BioComponent } from './bio/bio.component';
import * as CanvasJSAngularChart from '../assets/canvasjs.angular.component';
var CanvasJSChart = CanvasJSAngularChart.CanvasJSChart;
import { VideoCallComponent } from './video-call/video-call.component';
import { CallinfoDialogComponent } from './callinfo-dialog/callinfo-dialog.component';

import { MatButtonModule } from '@angular/material/button'
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from "@angular/material/form-field"
import { MatInputModule } from "@angular/material/input"
import { ClipboardModule } from '@angular/cdk/clipboard';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { VideoCall2Component } from './video-call2/video-call2.component';
import { ShowPrescriptionComponent } from './show-prescription/show-prescription.component';
import { ShowDietComponent } from './show-diet/show-diet.component';

@NgModule({
  declarations: [
    AppComponent,
    AppointmentsComponent,
    HomeComponent,
    LoginComponent,
    RegisterComponent,
    LogoutComponent,
    DoctorDashboardComponent,
    UserDashboardComponent,
    DoctorLoginComponent,
    CalorieTrackerComponent,
    DietForUserComponent,
    DoctorListComponent,
    ExpPipe,
    PrescriptionComponent,
    DietComponent,
    UserRegisterComponent,
    ForgotPasswordUserComponent,
    ResetPasswordComponent,
    ForgotPasswordDoctorComponent,
    ResetPasswordDoctorComponent,
    DoctorAppointmentsComponent,
    BioComponent,
    CanvasJSChart,
    VideoCallComponent,
    CallinfoDialogComponent,
    VideoCall2Component,
    ShowPrescriptionComponent,
    ShowDietComponent,
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule,
    FormsModule,
    HttpClientModule,
    SocialLoginModule,
    NgbModule,
    Ng2SearchPipeModule,
    BrowserAnimationsModule,
    // OverlayModule,
    ReactiveFormsModule,
    ToastrModule.forRoot(),
    CommonModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    ClipboardModule,
    MatSnackBarModule
  ],
  providers: [
    {
      provide: 'SocialAuthServiceConfig',
      useValue: {
        autoLogin: false,
        providers: [
          {
            id: GoogleLoginProvider.PROVIDER_ID,
            provider: new GoogleLoginProvider(
              '795757124746-vpemlpspuhs0677sm74a9flslnsalgcn.apps.googleusercontent.com'
            ),
          },
        ],
        onError: (err) => {
          console.error(err);
        },
      } as SocialAuthServiceConfig,
    },
    DatePipe,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
