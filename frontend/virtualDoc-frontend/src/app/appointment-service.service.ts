import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';



@Injectable({
  providedIn: 'root'
})
export class AppointmentServiceService {
  
  constructor(private httpClient: HttpClient) { 
    
  }
  getAllAppointments(): any {
    return this.httpClient.get('/getAllAppointments');
  }
  getMyAppointments(userId : any){
    return this.httpClient.get("/getAppointmentsByUserId/"+userId).toPromise();
  }
  
  getDoctorAppointments(docId: any){
    return this.httpClient.get("/getAppointmentsByDocId/"+docId).toPromise();
  }

  getAppointmentById(appointmentId : any){
    return this.httpClient.get("/getAppointmentById/"+appointmentId).toPromise();
  }

  acceptCancelAppointment(appointment : any){
    return this.httpClient.put("/updateAppointment", appointment).toPromise();
  }
  completeAppointment(appointment: any){
    return this.httpClient.put("/updateAppointment", appointment).toPromise();
  }
  
}
