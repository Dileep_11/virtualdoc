import { DatePipe, formatDate } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ColdObservable } from 'rxjs/internal/testing/ColdObservable';
import { AppointmentServiceService } from '../appointment-service.service';
import { UserService } from '../user.service';

@Component({
  selector: 'app-appointments',
  templateUrl: './appointments.component.html',
  styleUrls: ['./appointments.component.css']
})
export class AppointmentsComponent implements OnInit{
  appointments:any;
  t_appointments: any;
  p_appointments: any;
  title:any;
  appointment : any;
  appointmentID: any;
  emailId: any;
  userId: any;
  
  docId: any;
  showRows:any;
 
 



  constructor(private service: AppointmentServiceService, private router : Router,
    private userService : UserService, public datepipe: DatePipe) {
    
  }
  async ngOnInit() {
    
    this.title="ALL APPOINTMENTS";
    this.emailId=localStorage.getItem("userid");
    console.log(this.emailId);
    await this.userService.getUserId_p(this.emailId).then((data: any) => {this.userId = data; console.log("user Id :"+this.userId)});
    console.log(this.userId);
   
    await this.service.getMyAppointments(this.userId).then((data: any) => {this.appointments = data;});
    console.log(this.appointments);
    this.showRows=this.appointments;
    
    
  }
  pending(){
    this.title="PENDING APPOINTMENTS";

    this.p_appointments=[];
    this.appointments.forEach((element: any) => {
      if(element.status=="waiting"||element.status=="approved"){
        let appDate=formatDate(element.appDate,'yyyy-MM-dd','en_US')
        let todaysDate=formatDate(new Date(),'yyyy-MM-dd','en_US');
        if(element.status=="approved" && todaysDate<=appDate){
          this.p_appointments.push(element);
        }
      }
    });
    // this.p_appointments.forEach((element:any)=>{
    //   console.log(element);
    // })
    this.showRows=this.p_appointments;


  }
  todays(){
    this.title="TODAY'S APPOINTMENTS";

    this.showRows=3;
    this.t_appointments=[];
    this.appointments.forEach((element: any) => {
      let date=this.datepipe.transform(new Date(), "yyyy-MM-dd");
      if(element.appDate==date){
        this.t_appointments.push(element);
      }
    });
    this.showRows=this.t_appointments;

    // this.t_appointments.forEach((element:any)=>{
    //   console.log(element);
    // })
  }

  all(){
    this.title="ALL APPOINTMENTS";
    this.showRows=this.appointments;
  }

  joinMeeting(appointment: any){
    this.router.navigateByUrl("video2");
  }

  async showPrescription(appointment:any){
    console.log("appointment.appointmentId");
    localStorage.setItem("appointment",JSON.stringify(appointment));
    
    //console.log(JSON.parse());
    this.router.navigateByUrl("/showPrescription");
  }

  showDiet( appointment: any){
    localStorage.setItem("appointment",JSON.stringify(appointment));
    this.router.navigateByUrl("/showDiet");

  }
   

}
