import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CalorieTrackService {
  data: any;

  constructor(private httpClient : HttpClient) { 
    this.data={};
  }

  fetchCalories(foodItem : any) : any{
    return this.httpClient.get('https://api.nal.usda.gov/fdc/v1/foods/search?query='+foodItem+'&pageSize=1&api_key=wNxP2RfBXrx3amU5HypuuEWUtSSgeRErZMcU5LFA').toPromise();
  }

  addDiet(diet: any): any{
    console.log(diet);
    return this.httpClient.post("/registerPCTrack",diet);
  }

  getAllDiets(){
    return this.httpClient.get("/getAllPCTracks");
  }

  
}

