import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { CalorieTrackService } from '../calorie-track.service';
import { UserService } from '../user.service';

@Component({
  selector: 'app-calorie-tracker',
  templateUrl: './calorie-tracker.component.html',
  styleUrls: ['./calorie-tracker.component.css']
})
export class CalorieTrackerComponent implements OnInit{
  public isCollapsed = false;
  currUserId: any;
  BF: any;
  LCH : any;
  DNR : any;
  SNK : any;
  showdiv: any;
  BF_name : any;
  total:any;
  LCH_name : any;
  DNR_name : any;
  SNK_name : any;
  nutrients : any;
  len: any;
  visible_bf: any;
  visible_l: any;
  visible_d: any;
  visible_s: any;
  data: any
  count: any;

  BF_cals: any;
  L_cals: any;
  DNR_cals: any;
  SNK_cals: any;

  chartOptions: any;

  constructor(private service : CalorieTrackService, public datepipe: DatePipe, private userService : UserService){
    this.visible_bf=true;
    this.visible_l= true;
    this.visible_d= true;
    this.visible_s= true;
    this.showdiv=false;

  }


  public chartClicked(e: any): void {
    console.log(e);
  }

  public chartHovered(e: any): void {
    console.log(e);
  }

  ngOnInit(): void {
    this.userService
      .getUserId(localStorage.getItem('userid'))
      .subscribe((response) => {
        this.currUserId = response;
      });
    
  }

  async trackCalories(calorieForm: any){

    this.showdiv=true;

    //breakfast
    await this.service.fetchCalories(calorieForm.breakfast).then((data: any)=>{
      this.nutrients=data;
    });
    this.BF_name=this.nutrients.foods[0].description
    this.BF=this.printing();
    this.BF_cals=this.BF.Energy;
    

    

    //lunch
    await this.service.fetchCalories(calorieForm.lunch).then((data: any)=>{
      this.nutrients=data;
    });
    this.LCH_name=this.nutrients.foods[0].description
    this.LCH=this.printing();
    this.L_cals=this.LCH.Energy;

    //dinner
    await this.service.fetchCalories(calorieForm.dinner).then((data: any)=>{
      this.nutrients=data;
    });
    this.DNR_name=this.nutrients.foods[0].description
    this.DNR=this.printing();
    this.DNR_cals=this.DNR.Energy;
    
    //snack
    console.log("SNACK ");
    await this.service.fetchCalories(calorieForm.snack).then((data: any)=>{
      this.nutrients=data;
    });
    this.SNK_name=this.nutrients.foods[0].description
    // await this.printing().then((data : any)=>{
    //   this.SNK=data;
    // });
    this.SNK= this.printing();
    this.SNK_cals=this.SNK.Energy;


    // code to enter the calories of the day into the database on perticular day

    let latest_date =this.datepipe.transform(new Date(), 'yyyy-MM-dd');

    console.log(this.BF_cals+" "+this.L_cals+" "+this.DNR_cals+" "+this.SNK_cals);
    this.total=0;
    if(this.BF_cals){
      this.total=this.total+Number(this.BF_cals.split(" ")[0]);
    }
    if(this.L_cals){
      this.total=this.total+Number(this.L_cals.split(" ")[0]);
    }
    if(this.DNR_cals){
      this.total=this.total+Number(this.DNR_cals.split(" ")[0]);
    }
    if(this.SNK_cals){
      this.total=this.total+Number(this.SNK_cals.split(" ")[0]);
    }
    // this.total=Number(this.BF_cals.split(" ")[0])+Number(this.L_cals.split(" ")[0])+Number(this.DNR_cals.split(" ")[0])
    // +Number(this.SNK_cals.split(" ")[0]);

    localStorage.setItem("calories", this.total);
    let data={
                "calorieCount": this.total,
                "date": latest_date,
                "user": {"userId": this.currUserId}
             };

    this.service.addDiet(data).subscribe((data:any)=>{});

  }

   printing(){
    this.len=this.nutrients.foods[0].foodNutrients.length;
    console.log(this.nutrients.foods[0].description);
    let d: { [name: string]: string } = {};
    var key,value, nutrientNo;
    var temp=[{ y: 10, name: "Legal" }];
    
    
    for(let i=0;i<this.len;i++){
      this.count++;
      nutrientNo=this.nutrients['foods'][0]['foodNutrients'][i]['nutrientNumber']
      if(nutrientNo<292){
        key=JSON.stringify(this.nutrients.foods[0].foodNutrients[i].nutrientName).replaceAll('"','');
      value=JSON.stringify(this.nutrients.foods[0].foodNutrients[i].value)+" "
                  +JSON.stringify(this.nutrients.foods[0].foodNutrients[i].unitName).replaceAll('"','');  
      var x={y:Number(value), name:String(key)};
      temp.push(x);
      // this.chartOptions.data[0].dataPoints.push(x);
      d[key]=value;
      // this.SNK=d;

      } 
    }

    // await new Promise(resolve => setTimeout(resolve, 2000));

    // location.reload();
    // if(this.count==this.len-1){
      this.chartOptions={
        animationEnabled: true,
        title:{
        text: "ESSENCIAL NUTRIENTS"
        },
        data: [{
        type: "doughnut",
        yValueFormatString: "#,###.##",
        indexLabel: "{name}",
        dataPoints: temp
        }]
      }
    
    
    console.log(this.chartOptions);


    return d;
  }

  toggleCollapse(value : any): void {
    if(value==1){
    this.visible_bf = !this.visible_bf;
    this.visible_l = true;
    this.visible_d = true;
    this.visible_s = true;
    }
    else if(value==2){
      this.visible_bf = true;
      this.visible_l = !this.visible_l;
      this.visible_d = true;
      this.visible_s = true;
    }
    else if(value==3){
      this.visible_bf = true;
      this.visible_l = true;
      this.visible_d = !this.visible_d;
      this.visible_s = true;
    }
    else{
      this.visible_bf = true;
      this.visible_l = true;
      this.visible_d = true;
      this.visible_s = !this.visible_s;
    }
  }



}
