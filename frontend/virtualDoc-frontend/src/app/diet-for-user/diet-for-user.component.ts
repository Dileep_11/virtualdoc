import { Component } from '@angular/core';
import { UserService } from "../user.service";

@Component({
  selector: 'app-diet-for-user',
  templateUrl: './diet-for-user.component.html',
  styleUrls: ['./diet-for-user.component.css']
})
export class DietForUserComponent {
  emailid=localStorage.getItem("userid");
  userid:any;
  diets:any=[];

  constructor(private service: UserService ){
    // this.service.getUserId(this.emailid).subscribe((data: any) => {this.userid = data;});
    // console.log(this.userid);
  }

  async ngOnInit(){
    await this.service
      .getUserId_p(localStorage.getItem('userid'))
      .then((response) => {
        this.userid = response;
      });
      console.log(this.userid)

    await this.service.getDietForUser_p(this.userid).then((data: any)=>{
      this.diets=data;
      // console.log("diets",this.diets);
    });
    console.log(this.diets);

  }

  showDiet() {
     this.service.getDietForUser(this.userid).subscribe((data: any) => {
      this.diets = data;
      console.log(this.diets);
    });
     
   }

  

}
