import { Component } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AppointmentServiceService } from '../appointment-service.service';
import { DoctorService } from '../doctor.service';
@Component({
  selector: 'app-diet',
  templateUrl: './diet.component.html',
  styleUrls: ['./diet.component.css']
})
export class DietComponent {
  isSet:boolean=false;
  diet:any;
  appointment: any;
  constructor(private toastr: ToastrService,private service: DoctorService, private router : Router, private appService : AppointmentServiceService){
    this.appointment=localStorage.getItem("appointment");
    this.appointment=JSON.parse(this.appointment);
    this.diet = {breakFast:"",lunch:"",dinner:"",appointment:{appointmentID:this.appointment.appointmentID},user:{userId : this.appointment.user.userId},doctor:{docId : this.appointment.doctor.docId}};
  }
  setDiet(diet: any) {
    console.log(diet);
    this.diet.breakFast=diet.breakFast;
    this.diet.lunch=diet.lunch;
    this.diet.dinner=diet.dinner;
    console.log(this.diet);
    this.service.registerDiet(this.diet).subscribe((data: any)=>{console.log(data)});
    this.toastr.success('Diet Plan Registered Successfully');
    this.appointment.status="completed"
    this.appService.completeAppointment(this.appointment);
    this.router.navigateByUrl("/doctorAppointments");
  }
}

