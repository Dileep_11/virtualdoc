import { DatePipe, formatDate } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppointmentServiceService } from '../appointment-service.service';
import { DoctorService } from '../doctor.service';
import { LoginService } from '../login.service';
import { NotificationService } from '../notification.service';

@Component({
  selector: 'app-doctor-appointments',
  templateUrl: './doctor-appointments.component.html',
  styleUrls: ['./doctor-appointments.component.css']
})
export class DoctorAppointmentsComponent implements OnInit{
  appointments:any;
  t_appointments: any;
  p_appointments: any;
  r_appointments: any;
  title:any;
  appointment : any;
  appointmentID: any;
  emailId: any;
  doctorId: any;
  mailcontent: any;
  
  docId: any;
  showRows:any;
  today=this.datepipe.transform(new Date(), "yyyy-MM-dd");
 



  constructor(private service: AppointmentServiceService,
     private doctorService : DoctorService, public datepipe: DatePipe,
     private notifyService : NotificationService, 
     private router : Router, private loginService : LoginService) {

      this.mailcontent = {recipient:"",msgBody:"",subject:""};
    
  }
  async ngOnInit() {

    
    this.title="ALL APPOINTMENTS";
    this.emailId=localStorage.getItem("doctorid");
    
    // console.log(this.emailId);
    await this.doctorService.getDocIdbyEmail(this.emailId).then((data: any) => {this.doctorId = data; console.log("doctor Id :"+this.doctorId)});
    // console.log(this.doctorId);
   
    await this.service.getDoctorAppointments(this.doctorId).then((data: any) => {this.appointments = data;});
    // console.log(this.appointments);
    this.showRows=this.appointments;
    
    
  }
  pending(){
    this.title="PENDING APPOINTMENTS";

    this.p_appointments=[];
    this.appointments.forEach((element: any) => {
      let appDate=formatDate(element.appDate,'yyyy-MM-dd','en_US')
      let todaysDate=formatDate(new Date(),'yyyy-MM-dd','en_US');
      if(element.status=="approved" && todaysDate<=appDate){
        this.p_appointments.push(element);
      }

    });
    this.showRows=this.p_appointments;


  }

  requested(){
    this.title="REQUESTED APPOINTMENTS";

    this.r_appointments=[];
    this.appointments.forEach((element: any) => {
      if(element.status=="waiting"){
        this.r_appointments.push(element);
      }
    });
    this.showRows=this.r_appointments;

  }
  todays(){
    this.title="TODAY'S APPOINTMENTS";

    this.showRows=3;
    this.t_appointments=[];
    this.appointments.forEach((element: any) => {
      if(element.appDate==this.today){
        this.t_appointments.push(element);
      }
    });
    this.showRows=this.t_appointments;

  }

  all(){
    this.title="ALL APPOINTMENTS";
    this.showRows=this.appointments;
  }

  async acceptAppointment(appointmentId: any){

    this.appointments.forEach((appointment: any) => {
      if(appointment.appointmentID==appointmentId){
        appointment.status="approved";
        this.appointment=appointment;
      }
    });

    await this.service.acceptCancelAppointment(this.appointment).then((data: any)=>{
      console.log("Appointment Accepted");
    }).catch((error) => {
      console.log('Promise accepted with ' + JSON.stringify(error.error.text));
    });

    this.notifyService.showSuccess('Approved Appointment Id :'+this.appointment.appointmentID, 'virtualdoc.com');


    this.mailcontent.recipient = this.appointment.user.emailId;
    this.mailcontent.msgBody = "Hi "+ this.appointment.user.emailId+",\nYour Appointment with Dr. " + this.appointment.doctor.name +" was accepted by the doctor. \n\n The Appointment is scheduled on : \nDate : "+ 
        this.appointment.appDate +" \nTime : "+ this.appointment.appFromTime +"\nRequesting you to keep all the previous medical records(if any) handy.\n\nThank You, \nTeam VirtualDoc";
    this.mailcontent.subject="Appointment Approved";
    this.doctorService.sendEmailOTP(this.mailcontent).subscribe((data : any)=>{});

    

  }

  async cancelAppointment(appointmentId: any){

    this.appointments.forEach((appointment: any) => {
      if(appointment.appointmentID==appointmentId){
        appointment.status="cancelled";
        this.appointment=appointment;
      }
    });

    await this.service.acceptCancelAppointment(this.appointment).then((data: any)=>{
      console.log("Appointment cancelled");
    }).catch((error) => {
      console.log('Promise accepted with ' + JSON.stringify(error.error.text));
    });

    this.notifyService.showError('Cancelled Appointment Id : '+this.appointment.appointmentID, 'virtualdoc.com');

    this.mailcontent.recipient = this.appointment.user.emailId;
    this.mailcontent.msgBody = "Hi "+ this.appointment.user.emailId+",\nYour Appointment with Dr. " + this.appointment.doctor.name +".\n was Cancelled by the Doctor. \nYou can book another appointment for a new date.\n\nThank You, \nTeam VirtualDoc";
    this.mailcontent.subject="Appointment Cancelled";
    this.doctorService.sendEmailOTP(this.mailcontent).subscribe((data : any)=>{});

  }

  async joinMeeting(appointment: any){
    localStorage.setItem("appointment",JSON.stringify(appointment))
    // this.doctorService.setAppointment(appointment);
    this.router.navigateByUrl("video");
  }

  viewPrescription(appointment: any){
    localStorage.setItem("appointment",JSON.stringify(appointment));
    this.router.navigateByUrl("/showPrescription");
  }

  viewDiet(appointment: any){
    localStorage.setItem("appointment",JSON.stringify(appointment));
    this.router.navigateByUrl("/showDiet");

  }

}
