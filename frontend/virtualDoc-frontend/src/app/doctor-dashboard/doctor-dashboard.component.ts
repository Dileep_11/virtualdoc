import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModalConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AppointmentServiceService } from '../appointment-service.service';
import { DoctorService } from '../doctor.service';
import { LoginService } from '../login.service';


@Component({
  selector: 'app-doctor-dashboard',
  templateUrl: './doctor-dashboard.component.html',
  styleUrls: ['./doctor-dashboard.component.css']
})
export class DoctorDashboardComponent implements OnInit{
	emailid=localStorage.getItem("doctorid");
	docId:any;
	doctor:any;
	appointmentCount:any;
	pendingAppCount:any;
	todaysAppCount:any;
	today=this.datepipe.transform(new Date(), "yyyy-MM-dd");
	isLoggedUser:any;
	array: any=[];
	myPaitients: any=0;
	completedAppCount: any=0;
	approvedAppCount: any=0;
	todaysApprovedAppCount: any=0;
	todaysPendingAppCount: any=0;
	todaysCompletedAppCount: any=0;
	todaysApps: any=[];




  constructor(config: NgbModalConfig, private modalService: NgbModal , private loginService : LoginService,
	private appService : AppointmentServiceService,public datepipe: DatePipe, private router : Router, private service: DoctorService) {
		// customize default values of modals used by this component tree
		config.backdrop = 'static';
		config.keyboard = false;
		
	}
	async ngOnInit(){
		// console.log("hello")
		// this.loginService.getLoginStatus().subscribe((data: any) => {
		// 	this.isLoggedUser=data;
		// 	console.log("Status "+	this.isLoggedUser)
		//   });

		// if(this.isLoggedUser){
		// 	this.router.navigateByUrl("/doctorlogin")
		// }
		await this.service.getDoctorIdByEmail(this.emailid).then((data:any)=>{
			this.docId=data;	
			})
		await this.service.getDocbyId(this.docId).then((data:any)=>{
				this.doctor=data;	
			});
		await this.appService.getDoctorAppointments(this.docId).then((data: any) => {
			this.appointmentCount= data.length;
			this.pendingAppCount=0;
			this.todaysAppCount=0;
			data.forEach((element: any) => {
				// console.log(JSON.stringify(element));
				if(element.status=='waiting'){
					this.pendingAppCount+=1;
				}
				if(element.status=='completed'){
					this.completedAppCount+=1;
				}
				if(element.status=='approved'){
					this.approvedAppCount+=1;
				}
				if(element.appDate==this.today && element.status=='approved'){
			 		this.todaysApprovedAppCount+=1;
				}
				if(element.appDate==this.today && element.status=='waiting'){
					this.todaysPendingAppCount+=1;
			   	}
				if(element.appDate==this.today && element.status=='completed'){
					this.todaysCompletedAppCount+=1;
			    }
				if(element.appDate==this.today){
					this.todaysAppCount+=1;
					this.todaysApps.push(element);
			   	}

				this.array.push(element.user.userId);

			});
				console.log( this.todaysApps)

			console.log("Array is"+this.array);
			var unique = new Set(this.array);
			this.myPaitients=unique.size;
		});
		
	}

	open(content:any) {
		this.modalService.open(content);
	}
	updateDoctor(form: any) {
    this.service.updateDoctor(this.doctor).subscribe(data => {});
	}

}
