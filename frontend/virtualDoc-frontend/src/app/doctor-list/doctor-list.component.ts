import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NotificationService } from '../notification.service';
@Component({
  selector: 'app-doctor-list',
  templateUrl: './doctor-list.component.html',
  styleUrls: ['./doctor-list.component.css'],
})
export class DoctorListComponent implements OnInit {
  doctors: any;
  chkUser:any;
  public title = 'custom-search-filter-example';
  public searchedKeyword: string = '';
  closeResult = '';
  today: string = new Date().toISOString().split('T')[0];
  currDoctor: any;
  currUserId: any;
  message: string = '';

  appointment: any;
  ngOnInit(): void {
    this.service.getAllDoctors().subscribe((response:any) => {
      this.doctors = response;
      console.log(this.doctors);
    });
    this.service
      .getUserId(localStorage.getItem('userid'))
      .subscribe((response:any) => {
        this.currUserId = response;
      });
  
    
    
  }
  constructor(
    private service: UserService,
    private modalService: NgbModal,
    private notifyService: NotificationService
  ) {
    this.appointment = {
      appDate: '',
      appFromTime: '',
      status: '',
      doctor: { docId: '' },
      user: { userId: '' },
      prescription: null,
    };
  }
  bookAppointment(doctor: any, content:any) {
    this.service.getUserById(this.currUserId).then((response:any)=>{
      this.chkUser=response;
      console.log(this.chkUser);
      if(this.chkUser.password!=""){
        this.open(content);
      }
      else{
      this.notifyService.showError("Please fill all your details using Edit Profile Button", 'virtualdoc.com');

      }
  
          });
    this.currDoctor = doctor;
  }

  async addAppointment(appointmentForm: any) {
    this.appointment.appDate = appointmentForm.appDate;
    this.appointment.appFromTime = appointmentForm.appFromTime + ':00';
    this.appointment.status = 'waiting';
    this.appointment.doctor.docId = this.currDoctor.docId;
    this.appointment.user.userId = this.currUserId;
    this.appointment.prescription = null;
    await this.service
      .addAppointment(this.appointment)
      .then((response: any) => {})
      .catch((error:any) => {
        console.log(
          'Promise accepted with ' + JSON.stringify(error.error.text)
        );
        this.message = JSON.stringify(error.error.text);
      });
      console.log(this.message.length)
      if (this.message.length===34) {
      this.notifyService.showSuccess('Appointment Booked Successfully!', 'virtualdoc.com');
    } else {
      this.notifyService.showError(this.message, 'virtualdoc.com');
    }
  }

  open(content: any) {
      this.modalService
      .open(content, { ariaLabelledBy: 'modal-basic-title' })
      .result.then(
        (result) => {
          this.closeResult = `Closed with: ${result}`;
        },
        (reason) => {
          this.closeResult = `Dismissed`;
        }
      )
    }

}
