import { SocialAuthService } from '@abacritt/angularx-social-login';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DoctorService } from '../doctor.service';
import { LoginService } from "../login.service";
import { NotificationService } from '../notification.service';

@Component({
  selector: 'app-doctor-login',
  templateUrl: './doctor-login.component.html',
  styleUrls: ['./doctor-login.component.css']
})
export class DoctorLoginComponent implements OnInit{
  user: any;
  loggedIn: any;
  emailId: any;
  password : any;
  constructor(private authService: SocialAuthService, private router : Router, private service : DoctorService, private servicelogin: LoginService,private notifyService: NotificationService) { 
    this.user={name:'',password:'',education:'',doj:'',speciality:'',gender:'',appointmentFee:0.0,email:''};
  }

  ngOnInit() {
    this.authService.authState.subscribe(async (response) => {
      this.user.email = response.email;
      this.user.name = response.name;
      this.user.password='';
      this.user.doj='';
      this.user.gender='';
      this.user.education='';
      this.user.speciality='';
      this.loggedIn = (response != null);
      console.log(this.user.email);
      
      if(this.user){
        await this.service.getDocIdbyEmail(response.email).then((data:any)=>{this.user.userId=data}).catch((error:any)=>{console.log(error)});
       
        let Id:any;
        //alert("Hi! "+user.name+" \nSuccessfully Logged In as "+user.email);
        localStorage.setItem("doctorId", response.email);
        this.servicelogin.setUserLoggedIn();
        await this.service.getDocIdbyEmail(this.user.email).then((data : any)=>{
          Id=data;
        }).catch((error:any)=>{console.log(error)});
        console.log(Id);
        if(Id==0){
          await this.service.updateDoctor_p(this.user).then((data:any) => {console.log(data);}).catch((error:any) => {
            console.log(
              'Promise accepted with ' + JSON.stringify(error.error.text)
            );
          });;
        }
        this.router.navigate(["doctorDashboard"]);

      }
    });
    
  }
  async submitForm(loginFrom : any){
    console.log(loginFrom);
    
    await this.service.getDoctor(loginFrom).then((data: any) => {this.user = data; console.log(data);});
    console.log(this.user);
    localStorage.setItem("doctorid",loginFrom.emailId);

    if (this.user != null) {
      //alert(this.user.name+' Successfully LoggedIn');
      this.servicelogin.setUserLoggedIn();
      this.router.navigate(["doctorDashboard"]);  // Change to doctor dashboard
      this.notifyService.showSuccess('Welcome '+this.user.name, 'virtualdoc.com');
    }else{
      this.notifyService.showError('Invalid Credentials!', 'virtualdoc.com');
      this.router.navigate(['doctorlogin']);
    }
  }
}
