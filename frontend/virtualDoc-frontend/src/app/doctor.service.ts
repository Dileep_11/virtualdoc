import { HttpClient,HttpHeaders  } from '@angular/common/http';
import { Injectable, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class DoctorService {
  
  


  readonly APIUrl = "http://localhost:8080/"

  constructor(private httpClient: HttpClient) { 

  }
  

  

  updateDoctor(doctor :any) {
	  return this.httpClient.put('/updateDoctor',doctor);
  }
  getDoctor(doctor: any) {
    return this.httpClient.get("/doctorValidate/" + doctor.emailId + "/" + doctor.password).toPromise();
  }
  ngOnInit(): void {
    throw new Error('Method not implemented.');
  }
  addPrescription(content :any){
    console.log("strContent");
    console.log(content);
    return this.httpClient.post("/registerPrescription",content).toPromise();
  }
  sendPrescriptionMail(content: any){
    console.log("test2"+content.recipient);
    return this.httpClient.post("/sendMail",content).toPromise();
  }
  registerDiet(diet: any){
    console.log("ok3"+diet.breakfast);
    return this.httpClient.post("/registerDiet",diet);
  }

  PostDoctor(doctor: any) {
    return this.httpClient.post('/registerDoctor',doctor)
  }

  

  sendEmailOTP(content:any){
    return this.httpClient.post("/sendOTPemail",content);
  }

  getDocIdbyEmail(email:any){
    return this.httpClient.get('/getDoctorIdByEmailId/'+email).toPromise();
  }

  getDocbyId(docid:any){
    return this.httpClient.get('/getDoctorById/'+docid).toPromise();
  }

  PostDoctor_p(doctor: any) {
    return this.httpClient.post('/registerDoctor',doctor).toPromise();
  }

  sendMeetingId(content: any){
    return this.httpClient.post("/sendMeetingId",content).toPromise();
  }

  
  
  

  registerDoctor(doctor:any){
    return this.httpClient.post('/registerDoctor',doctor);
  }

 
  getDoctorIdByEmail(docId:any){
    return this.httpClient.get("/getDoctorIdByEmailId/"+docId).toPromise();
  }

  updateDoctor_p(doctor :any) {
	  return this.httpClient.put('/updateDoctor',doctor).toPromise();
  }

}
