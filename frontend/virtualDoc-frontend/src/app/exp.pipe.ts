import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'exp',
})
export class ExpPipe implements PipeTransform {
  transform(value: any) {
    const date = new Date(value);
    const today = new Date();
    return today.getFullYear() - date.getFullYear();
  }
}
