import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { NotificationService } from '../notification.service';
import { UserService } from '../user.service';

@Component({
  selector: 'app-forgot-password-user',
  templateUrl: './forgot-password-user.component.html',
  styleUrls: ['./forgot-password-user.component.css']
})
export class ForgotPasswordUserComponent {
  userid:any;
  otp:any;
  timestamp: any;
  mailcontent:any;
  minutes = 5;
  seconds = 0;
  interval:any;
  constructor( private router : Router, private service : UserService,private notifyService: NotificationService){
    this.mailcontent = {recipient:"",msgBody:"",subject:"OTP to reset password"};

  }
  ngOnInIt(){}
  generateOTP(): string {
    return Math.floor(Math.random() * 10000).toString();
  }
  isOtpValid(otp: string, timestamp: number): boolean {
    // check if the OTP is still valid (within 5 minutes)
    return (Date.now() - timestamp) <= (5 * 60 * 1000);
  }
   startTimer() {
    this.interval = setInterval(() => {
      if(--this.seconds < 0) {
        this.seconds = 59;
        if(--this.minutes < 0) {
          this.minutes = 0;
          this.seconds = 0;
          clearInterval(this.interval);
        }
      }
    }, 1000);
  }
  submitOTP(otpForm:any){
    if(this.otp==otpForm.otp && this.isOtpValid(this.otp,this.timestamp)){
      this.notifyService.showSuccess('OTP verification successfull','virtualdoc.com');
      this.router.navigate(["/ResetpasswordUser"]);
    }
    else{
      this.notifyService.showError('OTP verification Unsuccessfull!','virtualdoc.com');
    }
  }
  async submitForm(loginForm:any){
    await this.service.getUserId_p(loginForm.emailId).then((data: any) => {this.userid = data;});
    if(this.userid!= 0){
      localStorage.setItem('ForgotPassEmail',loginForm.emailId);
      this.otp = this.generateOTP();
      this.startTimer();
      this.timestamp = Date.now();

      this.mailcontent.recipient = loginForm.emailId;
      this.mailcontent.msgBody = "Hi "+ loginForm.emailId+",\nOtp to reset your password is " + this.otp +".\nThis otp is only valid for 5 minutes\n\nThank You,\nTeam VirtualDoc";

      this.service.sendEmailOTP(this.mailcontent).subscribe((data:any)=>{
        console.log(data);
     });
      
      this.notifyService.showInfo('OTP is sent to email','virtualdoc.com');
    }
    else{
      this.notifyService.showError('Sorry, EmailId is not registered!', 'virtualdoc.com');
    }
  }
}
