import { Component, OnInit } from '@angular/core';
import { LoginService } from '../login.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {
  isLoggedIn: any;
  constructor(private service: LoginService) {
    
  }

  ngOnInit(): void {
    this.service.getLoginStatus().subscribe((data: any) => {
      this.isLoggedIn = data;
      console.log("-->"+data);
    });
    console.log("==>"+localStorage.getItem("userid") +" "+localStorage.getItem("doctorid"))
    if(localStorage.getItem("userid")==null&&localStorage.getItem("doctorid")==null){
      this.isLoggedIn=false;
    }
  }
}
