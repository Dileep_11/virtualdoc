import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
@Injectable({
  providedIn: 'root',
})
export class LoginService {
  isUserLogged: boolean;
  loginStatus: Subject<any>;

  constructor(private httpClient: HttpClient) {
    this.isUserLogged = false;
    this.loginStatus = new Subject();
  }

  getLoginStatus(): any {
    // console.log("login Status"+JSON.stringify(this.loginStatus));
    return this.loginStatus.asObservable();
  }
  getLoginStatus_p(): any {
    return this.loginStatus.asObservable().toPromise();
  }

  setUserLoggedIn() {
    this.isUserLogged = true;
    this.loginStatus.next(this.isUserLogged);
  }
  setUserLoggedOut() {
    this.isUserLogged = false;
    this.loginStatus.next(this.isUserLogged);
  }
  getUserLogged() {
    return this.isUserLogged;
  }
}
