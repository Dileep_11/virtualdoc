import { SocialAuthService } from '@abacritt/angularx-social-login';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../user.service';
import { LoginService } from "../login.service";
import { NotificationService } from '../notification.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit{
  user: any;
  loggedIn: any;
  emailId: any;
  password : any;
  constructor(private authService: SocialAuthService, private router : Router, private service : UserService, private servicelogin: LoginService,private notifyService: NotificationService) { 
    this.user={emailId:'',name:'',password:'',dob:'',gender:'',mobile:''};
  }

  ngOnInit() {
    this.authService.authState.subscribe(async (response) => {

      this.user.emailId = response.email;
      this.user.name = response.name;
      this.loggedIn = (response != null);
      

      console.log(this.user.email);
      
      if(this.user){
        await this.service.getUserIdByEmailId(response.email).then((data:any)=>{this.user.userId=data}).catch((error:any)=>{console.log(error)});
        if(this.user.userId!=0){
        localStorage.setItem("userid",response.email);
        this.servicelogin.setUserLoggedIn();
          this.router.navigate(["userDashboard"]);
        this.notifyService.showSuccess('Welcome '+response.email, 'virtualdoc.com');

        }
        else{
        this.user.password='';
        this.user.dob='';
        console.log(this.user.userId);
        //alert("Hi! "+user.name+" \nSuccessfully Logged In as "+user.email);
        this.notifyService.showSuccess('Welcome '+response.email, 'virtualdoc.com');
        localStorage.setItem("userid",response.email);
        this.servicelogin.setUserLoggedIn();
        
        this.service.updateUser(this.user).subscribe((data:any) => {console.log(data);});
        this.router.navigate(["userDashboard"]);
        }
      }
    });
    
  }

 
  async submitForm(loginFrom : any){

    
    console.log(loginFrom);

    await this.service.getUser(loginFrom).then((data: any) => {this.user = data; console.log(data);});
    localStorage.setItem("userid",loginFrom.emailId);

    if (this.user != null) {
      //alert(this.user.name+' Successfully LoggedIn');
      this.servicelogin.setUserLoggedIn();
      this.router.navigate(["userDashboard"]);
      this.notifyService.showSuccess('Welcome '+this.user.name, 'virtualdoc.com');
    }else{
      //alert('Invalid Credentials!!!');
      this.notifyService.showError('Invalid Credentials!', 'virtualdoc.com');
      this.router.navigate(['login']);
    }
  }
}
