import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { SocialAuthService } from '@abacritt/angularx-social-login';
import { LoginService } from "../login.service";
import { NotificationService } from '../notification.service';
@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css']
})
export class LogoutComponent {
  constructor(private authService : SocialAuthService,private router : Router, private servicelogin: LoginService,private notifyService: NotificationService) {
  }

  ngOnInit(): void {
    this.authService.signOut();
    this.servicelogin.setUserLoggedOut();
    localStorage.clear();
    this.notifyService.showInfo('Logged out successfully!','virtualdoc.com')
    this.router.navigate(['/login']);
  }
}
