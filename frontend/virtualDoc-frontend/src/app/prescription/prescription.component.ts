import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';


//import { MatInputModule, MatButtonModule, MatIconModule } from '@angular/material';
//const { dialog } = require('electron').remote;

declare var require: any;



import * as pdfMake from "pdfmake/build/pdfmake";
import * as pdfFonts from "pdfmake/build/vfs_fonts";
import { DoctorService } from '../doctor.service';
const htmlToPdfmake = require("html-to-pdfmake");
(pdfMake as any).vfs = pdfFonts.pdfMake.vfs;
@Component({
  selector: 'app-prescription',
  templateUrl: './prescription.component.html',
  styleUrls: ['./prescription.component.css']
})
export class PrescriptionComponent implements OnInit{
  medRecords: any =[];
  cnt: number=1;
  presc: any;
  content: any={};
  appointment:any={};
  patientId:any;
  patientName:any;
  doctorId: any;
  doctorName:any;
  recipient:any;
  retAppointment: any;
  gappointment: any;
  showBtn: any;
  ngOnInit(): void {
  }
  constructor(private doctorService: DoctorService, private router: Router){
    this.retAppointment=localStorage.getItem("appointment");
    this.gappointment = JSON.parse(this.retAppointment);
    //console.log(this.appointment.user.userId);
    this.patientId = this.gappointment.user.userId;
    this.patientName = this.gappointment.user.name;
    this.doctorId =this.gappointment.doctor.docId;
    this.doctorName = this.gappointment.doctor.name;
    this.recipient = this.gappointment.user.emailId;
    this.showBtn=true;
  }
  today = new Date();
  

  addRecord() {
    this.medRecords.push({ recId: this.medRecords.length+1,medicine: '', dosage: '', instructions: '' });
    
  }

  deleteRecord(record: any) {
    const i = this.medRecords.findIndex((element: any) => {
      return record.recId === element.recId;
    });
    this.medRecords.splice(i, 1);
  }
  editRecord(){

  }
   sendPres(){
    let strMedRecord = "";
    let temp="";
    this.medRecords.forEach((medRecord: any) => {
      temp+=JSON.stringify(medRecord)+","
      strMedRecord += medRecord.medicine+","+medRecord.dosage+","+medRecord.instructions+"||";
    });
    

    // console.log(strMedRecord);
    this.presc = {recipient:this.recipient,
                  subject:"Prescription Details",
                  body: strMedRecord,
                  patientId:this.patientId,
                  patientName:this.patientName,
                  doctorId:this.doctorId,
                  doctorName:this.doctorName,
                  medRecords: this.medRecords
                };

    
    this.doctorService.sendPrescriptionMail(this.presc).then((data:any)=>{
        console.log("test3"+data);
     });
     console.log("hello"+this.gappointment.appointmentID);
     this.content = {content: strMedRecord,appointment:{appointmentID:this.gappointment.appointmentID}};
    //  console.log(this.content);
    //  console.log("HELLO"+temp);
     this.doctorService.addPrescription(this.content).then((data:any)=>{});
    this.showBtn=false;

    this.router.navigateByUrl("/diet");



  }





  // @ViewChild('pdfTable')
  // pdfTable!: ElementRef;
  // public exportPDF() {
  //   const pdfTable = this.pdfTable.nativeElement;
  //   var html = htmlToPdfmake(pdfTable.innerHTML);
  //   const documentDefinition = { content: html };
  //   pdfMake.createPdf(documentDefinition).download(); 
     
  // }
  // public convertToPDF()
  // {
  // var data = document.getElementById('contentToConvert')!;
  // html2canvas(data).then(async canvas => {
  // // Few necessary setting options
  // var imgWidth = 208;
  // var pageHeight = 295;
  // var imgHeight = canvas.height * imgWidth / canvas.width;
  // var heightLeft = imgHeight;
  
  // const contentDataURL = canvas.toDataURL('image/png')
  // let pdf = new jspdf('p', 'mm', 'a4'); // A4 size page of PDF
  // var position = 0;
  // pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)
  // pdf.save('new-file.pdf'); // Generated PDF
  // });
  
  
     
  // }


}
