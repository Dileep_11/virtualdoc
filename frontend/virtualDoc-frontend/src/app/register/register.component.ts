import { Component } from '@angular/core';
import { DoctorService } from '../doctor.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent {

  Alert=false;
  data:any;
  RegistrationSuccess: any = [];
  Registration :any;


  constructor(private service: DoctorService,private router:Router) { 
    
  }


  CreateUser(form:any) {
    console.log(form);
    this.service.PostDoctor(form).subscribe(data => {
      
      
    });
    this.router.navigate(['/login']);
  }

}
