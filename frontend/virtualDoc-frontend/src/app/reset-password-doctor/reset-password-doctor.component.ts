import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { DoctorService } from '../doctor.service';
import { NotificationService } from '../notification.service';

@Component({
  selector: 'app-reset-password-doctor',
  templateUrl: './reset-password-doctor.component.html',
  styleUrls: ['./reset-password-doctor.component.css']
})
export class ResetPasswordDoctorComponent {
  emailId:any;
  password:any;
  repassword:any;
  docid:any;
  doctor:any;
  constructor(private service:DoctorService,private router: Router,private notifyService: NotificationService){
    this.emailId = localStorage.getItem('ForgotPassEmail');

  }

  onInIt(){

  }

  async submitReset(ResetForm:any){
    await this.service.getDocIdbyEmail(this.emailId).then((data:any) => {this.docid=data;});
    await this.service.getDocbyId(this.docid).then((data:any) => {this.doctor=data;});
    
    if(this.doctor.password!=""){
      this.doctor.password=ResetForm.password;
      if(this.doctor.password!=""){
        this.service.updateDoctor(this.doctor).subscribe((data:any) => {console.log(data)});
        this.notifyService.showSuccess('Password Changed Successfully!','virtualdoc.com');
        this.router.navigate(['doctorlogin']); 
        localStorage.clear();
      }
    }
    else{
      this.notifyService.showError('Please update your password from profile section!', 'virtualdoc.com');
      this.router.navigate(['doctorlogin']); // Change to update details
      localStorage.clear();
    }
    
  }
}
