import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { NotificationService } from '../notification.service';
import { UserService } from '../user.service';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent {
  emailId:any;
  password:any;
  repassword:any;
  userid:any;
  user:any;
  constructor(private service:UserService,private router: Router,private notifyService: NotificationService){
    this.emailId = localStorage.getItem('ForgotPassEmail');

  }

  onInIt(){

  }

  async submitReset(ResetForm:any){
    await this.service.getUserId_p(this.emailId).then((data:any) => {this.userid=data;console.log(this.userid)});
    await this.service.getUserbyID(this.userid).then((data:any) => {this.user=data;console.log(this.user)});
    
    if(this.user.password!=""){
      this.user.password=ResetForm.password;
      if(this.user.password!=""){
        this.service.updateUser(this.user).subscribe((data:any) => {console.log(data)});
        this.notifyService.showSuccess('Password Changed Successfully!','virtualdoc.com');
        this.router.navigate(['login']); 
        localStorage.clear();
      }
    }
    else{
      this.notifyService.showError('Please update your password from profile section!', 'virtualdoc.com');
      this.router.navigate(['login']); // Change to update details
      localStorage.clear();

    }
    
  }

}
