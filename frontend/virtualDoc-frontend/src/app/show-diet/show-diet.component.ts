import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';

@Component({
  selector: 'app-show-diet',
  templateUrl: './show-diet.component.html',
  styleUrls: ['./show-diet.component.css']
})
export class ShowDietComponent implements OnInit{
  appointment: any;
  dietId : any;
  diets:any;
  constructor(private service : UserService){

  }
  async ngOnInit(){
    this.appointment=localStorage.getItem("appointment");
    this.appointment=JSON.parse(this.appointment);
    this.dietId=this.appointment.diet.dietID;
    console.log("diet id"+this.dietId);
     this.service
      .getDietById(this.dietId)
      .subscribe((data: any) => {
        this.diets = data;
        // console.log("diets",this.diets);
      });
  }
  

}
