import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';

@Component({
  selector: 'app-show-prescription',
  templateUrl: './show-prescription.component.html',
  styleUrls: ['./show-prescription.component.css']
})
export class ShowPrescriptionComponent implements OnInit{

  prescription: any;
  retAppointment:any;
  appointment:any;
  presId: any;
  today: Date = new Date();
  metaMedRecData:any={};
  medRecords:any=[];

  constructor(private userService: UserService){
  }

  async ngOnInit() {
    this.retAppointment = localStorage.getItem("appointment");
    this.appointment = JSON.parse(this.retAppointment);
    console.log(JSON.stringify(this.appointment));
    await this.userService.getPresIdByAppId(this.appointment.appointmentID).then((data:any)=>{
      
      this.presId = data;
      console.log("PRES id"+this.presId);
    }).catch((error)=>{
      console.log(error)
    });

    await this.userService.getPrescriptionById(this.presId).then((data: any)=>{
      console.log("Pres data");
      console.log(data);
      this.metaMedRecData = data;
    }).catch((error)=>{
      console.log(error)
    });;

    this.convertMetaData();
  }
  convertMetaData() {
    let str = [];
    let index;
    str = this.metaMedRecData.content.split("||");
    for (index = 0;index < str.length-1;index++) {
      let medRec=str[index].split(",");
      this.medRecords.push({medicine:medRec[0],dosage:medRec[1],instructions:medRec[2]});
    }
  }
}

