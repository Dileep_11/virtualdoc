import { Injectable, OnInit } from '@angular/core';
import firebase from 'firebase/compat/app';
import 'firebase/compat/auth';
import 'firebase/compat/firestore';
import { PhoneNumber } from './PhoneNumber';
import { WindowService } from './window.service';

const firebaseConfig = {
  apiKey: "AIzaSyBBwu1BcCl1RsNucXhQOGF3EXZ0O11tZnM",
  authDomain: "virdoc-13215.firebaseapp.com",
  projectId: "virdoc-13215",
  storageBucket: "virdoc-13215.appspot.com",
  messagingSenderId: "1038511674544",
  appId: "1:1038511674544:web:474f58afb5ad197b1bd775",
  measurementId: "G-DFK5M9TN83"
};

firebase.initializeApp(firebaseConfig);

const auth = firebase.auth();


@Injectable({
  providedIn: 'root'
})
export class SmsAuthService implements OnInit{
  windowRef: any;

  phoneNumber = new PhoneNumber()

  verificationCode: any;

  user: any;

  constructor(private win: WindowService) { }

  ngOnInit() {
    // this.windowRef = this.win.windowRef
    // this.windowRef.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container');

    // this.windowRef.recaptchaVerifier.render()
  }

  getRecaptchaVerifier(){
    return new firebase.auth.RecaptchaVerifier('recaptcha-container');
  }


  sendLoginCode( appVerifier:any, num:any) {

    
    firebase.auth().signInWithPhoneNumber(num, appVerifier)
            .then((result:any) => {

                return result;
                // this.windowRef.confirmationResult = result;

            })
            .catch( (error: any) => console.log(error) );

  }

  // verifyLoginCode() {
  //   this.windowRef.confirmationResult
  //                 .confirm(this.verificationCode)
  //                 .then( (result:any) => {

  //                   this.user = result.user;

  //   })
  //   .catch( (error:any) => console.log(error, "Incorrect code entered?"));
  // }


}
