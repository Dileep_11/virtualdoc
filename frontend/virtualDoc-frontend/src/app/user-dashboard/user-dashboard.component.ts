import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModalConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AppointmentServiceService } from '../appointment-service.service';
import { NotificationService } from '../notification.service';
import { UserService } from '../user.service';

@Component({
  selector: 'app-user-dashboard',
  templateUrl: './user-dashboard.component.html',
  styleUrls: ['./user-dashboard.component.css'],
})
export class UserDashboardComponent implements OnInit {
  currUserId: any;
  diets: any;
  calories: any;
  pCT: any=[];
  pCTs:any=[];
  latestpCT: any;
  doctorsCount: any;
  appointmentsCount: any;
  len:any;
  dietCount:any;
  bmi:any=0;

  emailid = localStorage.getItem('userid');
  userId: any;
  user: any;
  content: any;
  medRecords : any=[];
  MetaData:any={};
  height_f: any;
  height_i:any;
  weight: any;

  constructor(
    private router: Router,
    private service: UserService,
    config: NgbModalConfig,
    private modalService: NgbModal,
    private notifyService: NotificationService,
    private appservice: AppointmentServiceService
  ) {
    this.user = {
      userId: '',
      emailId: '',
      name: '',
      password: '',
      dob: '',
      gender: '',
      mobile: '',
    };
    // customize default values of modals used by this component tree
    config.backdrop = 'static';
    config.keyboard = false;
    this.calories = localStorage.getItem('calories');
    
  }

  async open(content: any) {
    await this.service.getUserIdByEmailId(this.emailid).then((data: any) => {
      this.userId = data;
    }).catch((error:any)=>{console.log(error)});
    await this.service.getUserById(this.userId).then((data: any) => {
      this.user = data;
    }).catch((error:any)=>{console.log(error)});
    this.modalService.open(content);
    console.log(this.user);
    console.log(this.userId);
  }
  updateUser(form: any) {
    this.user.userId = this.currUserId;
    this.user.emailId = form.emailId;
    this.user.name = form.name;
    this.user.dob = form.dob;
    this.user.gender = form.gender;
    this.user.password = form.password;
    this.user.mobile = form.mobile;
    console.log(form);
    console.log(this.user);
    console.log(this.emailid);
    this.service.updateUser(this.user).subscribe((data) => {});
    this.notifyService.showSuccess(
      'Your Details has been Updated!!',
      'virtualdoc.com'
    );
  }

  async ngOnInit() {


    await this.service
      .getUserId_p(localStorage.getItem('userid'))
      .then((response) => {
        this.currUserId = response;
      }).catch((error:any)=>{console.log(error)});

      await this.service.getLatestPrescriptionByUser(this.currUserId).then((data : any)=>{
        this.MetaData=data;
        this.convertMetaData();
      }).catch((error:any)=>{console.log(error)});
     

    await this.service
      .getLatestDietForUser_p(this.currUserId)
      .then((data: any) => {
        this.diets = data;
        // console.log("diets",this.diets);
      }).catch((error:any)=>{console.log(error)});

    this.service
      .getPatientCTByUserId(this.currUserId)
      .subscribe((response: any) => {
        this.pCT = response;
        console.log(this.pCT[0].date);
        this.len=response.length;

        this.pCT=this.pCT.reverse();
        this.latestpCT = response[0];
      });

      this.service.getAllDoctors().subscribe((response: any) => {
        this.doctorsCount = response.length;
        console.log(this.doctorsCount);
      });

      await this.service.getDietByUser(this.currUserId).then((response: any) => {
        this.dietCount = response.length;
        console.log(this.dietCount);
      }).catch((error:any)=>{console.log(error)});

      await this.appservice
      .getMyAppointments(this.currUserId)
      .then((data: any) => {
        this.appointmentsCount = data.length;
        console.log(this.appointmentsCount);
      }).catch((error:any)=>{console.log(error)})
      
    
    console.log("MED RECORDS"+JSON.stringify(this.MetaData))
    console.log(this.medRecords)
  }

  convertMetaData() {
    let str = [];
    let index;
    str = this.MetaData.content.split("||");
    for (index = 0;index < str.length-1;index++) {
      let medRec=str[index].split(",");
      this.medRecords.push({medicine:medRec[0],dosage:medRec[1],instructions:medRec[2]});
    }
  }

  viewDoctors() {
    this.router.navigateByUrl('/doctorlist');
  }

  calculate(){
    this.bmi=this.weight/(((this.height_f/3.281)+(this.height_i/39.37))**2)
  }
}
