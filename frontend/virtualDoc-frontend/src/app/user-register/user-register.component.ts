import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../user.service';
import { WindowService } from '../window.service';
import firebase from 'firebase/compat/app';
import 'firebase/compat/auth';
import 'firebase/compat/firestore';
import { NotificationService } from '../notification.service';

const firebaseConfig = {
  apiKey: "AIzaSyBBwu1BcCl1RsNucXhQOGF3EXZ0O11tZnM",
  authDomain: "virdoc-13215.firebaseapp.com",
  projectId: "virdoc-13215",
  storageBucket: "virdoc-13215.appspot.com",
  messagingSenderId: "1038511674544",
  appId: "1:1038511674544:web:474f58afb5ad197b1bd775",
  measurementId: "G-DFK5M9TN83"
};

firebase.initializeApp(firebaseConfig);

const auth = firebase.auth();

@Component({
  selector: 'app-user-register',
  templateUrl: './user-register.component.html',
  styleUrls: ['./user-register.component.css']
})
export class UserRegisterComponent implements OnInit{

  Alert=false;
  windowRef: any;
  verificationCode: any;
  takeOTP:any;
  check:any=false;
  userId:any;



  RegistrationSuccess: any = [];
  Registration :any;
  show:any;


  constructor(private service:UserService, private router: Router,private win: WindowService,private notifyService: NotificationService){
    this.show=true;
  }
  ngOnInit(): void {
    this.windowRef = this.win.windowRef
    this.windowRef.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container');

    this.windowRef.recaptchaVerifier.render()
    this.takeOTP=false;
  }

  async CreateUser(form:any) {
    console.log(form);
     await this.service.getUserIdByEmailId(form.emailId).then((data:any)=>{this.userId=data}).catch((error:any)=>{console.log(error)});
        if(this.userId!=0){
          this.notifyService.showError('Email Id Already Exists', 'virtualdoc.com');
        }else{

    this.service.postUser(form).subscribe((data:any) => {

    });
    this.router.navigate(['/login']);
  }

  }

  sendOTP(mobile : any){


    const appVerifier = this.windowRef.recaptchaVerifier;

    const num = "+91" + mobile.value;
    // const num = "919052353396";

    firebase.auth().signInWithPhoneNumber(num, appVerifier)
            .then((result:any) => {

                this.windowRef.confirmationResult = result;

            })
            .catch( (error: any) => console.log(error) );

    }

  verifyLoginCode(){
    this.windowRef.confirmationResult
                  .confirm(this.verificationCode)
                  .then( (result:any) => {
                    console.log("verified OTP");
                    this.notifyService.showSuccess('OTP verified Successfully','virtualdoc.com')
                    this.check=true;
    })
    .catch( (error:any) => {console.log(error, "Incorrect code entered?");this.notifyService.showError('Incorrect OTP entered','virtualdoc.com')} );
    this.show=false;
  }
}
