import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
@Injectable({
  providedIn: 'root',
})
export class UserService {

  getLatestPrescriptionByUser(currUserId: any) {
    return this.httpClient.get("/getLatestPrescriptionByUser/"+currUserId).toPromise();
  }
  constructor(private httpClient: HttpClient) {}
  postUser(user: any) {
    console.log(user);
    return this.httpClient.post('/registerUser', user);
  }

  registerUser(doctor: any) {
    return this.httpClient.post('/registerDoctor', doctor);
  }

  getPatientCTByUserId(userId: any) {
    return this.httpClient.get('/getPatientCTByUserId/' + userId);
  }

  getUser(user: any) {
    return this.httpClient
      .get('/userValidate/' + user.emailId + '/' + user.password)
      .toPromise();
  }

  getDietForUser(userID: any) {
    return this.httpClient.get('/getDietByUser/' + userID);
  }

  getDietForUser_p(userID: any) {
    return this.httpClient.get('/getDietByUser/' + userID).toPromise();
  }

  getLatestDietForUser_p(userID:any){
    return this.httpClient.get("/getLatestDietByUser/" + userID).toPromise();
  }

  getDietByUser(userID: any){
    return this.httpClient.get("/getDietByUser/" + userID).toPromise();
  }
  getDietById(dietID: any){
    return this.httpClient.get("/getDietById/" + dietID);
  }

  getUserId(emailId:any){
    return this.httpClient.get("/getUserIdByEmailId/"+ emailId);
  }

  getUserId_p(emailId: any) {
    return this.httpClient.get('/getUserIdByEmailId/' + emailId).toPromise();
  }

  addAppointment(appointment: any) {
    return this.httpClient.post('/createAppointment', appointment).toPromise();
  }
  getAllDoctors() {
    return this.httpClient.get('getAllDoctors');
  }

  sendEmailOTP(content: any) {
    return this.httpClient.post('/sendOTPemail', content);
  }

  getUserbyID(userid: any) {
    return this.httpClient.get('/getUserById/' + userid).toPromise();
  }

  updateUser(user: any) {
    return this.httpClient.put('/updateUser', user);
  }
  getUserById(userId: any) {
    return this.httpClient.get('getUserById/' + userId).toPromise();
  }
 
  
  getUserIdByEmailId(userId:any) {
    return this.httpClient.get("/getUserIdByEmailId/"+userId).toPromise();
  }
  getUserIdByEmailId_(userId:any) {
    return this.httpClient.get("/getUserIdByEmailId/"+userId);
  }

  getPresIdByAppId(appId:any){
    return this.httpClient.get("/getPresIdByAppId/"+appId).toPromise();
  }

  getPrescriptionById(presId:any){
    return this.httpClient.get("/getPrescriptionById/"+presId).toPromise();
  }
  
}
