import {
  Component,
  ElementRef,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { filter, switchMap } from 'rxjs/operators';
import { CallService } from '../call.service';
import {
  CallinfoDialogComponent,
  DialogData,
} from '../callinfo-dialog/callinfo-dialog.component';
import { DoctorService } from '../doctor.service';
@Component({
  selector: 'app-video-call',
  templateUrl: './video-call.component.html',
  styleUrls: ['./video-call.component.css'],
})
export class VideoCallComponent {
  public isCallStarted$: Observable<boolean>;
  private peerId: string;
  retAppointment:any;
  appointment:any;
  meetingDetails: any;
  @ViewChild('localVideo')
  localVideo!: ElementRef<HTMLVideoElement>;
  @ViewChild('remoteVideo')
  remoteVideo!: ElementRef<HTMLVideoElement>;

  constructor(
    public dialog: MatDialog,
    private callService: CallService,
    private router: Router,
    private doctorService: DoctorService
  ) {
    this.isCallStarted$ = this.callService.isCallStarted$;
    this.peerId = this.callService.initPeer();
    console.log('test' + this.peerId);
    this.sendId(this.peerId);
  }
   sendId(peerId: string) {
    this.retAppointment = localStorage.getItem("appointment");
    this.appointment = JSON.parse(this.retAppointment);
    this.meetingDetails = {recipient: this.appointment.user.emailId,
      subject: "Your Meeting details", 
      body:"Please find the meeting ID below to join the meeting \n\n Your meeting ID is : "+this.peerId};
     this.doctorService.sendMeetingId(this.meetingDetails).then((data:any)=>{});
  }
  ngOnInit(): void {
    this.callService.localStream$
      .pipe(filter((res) => !!res))
      .subscribe(
        (stream: MediaProvider | null) =>
          (this.localVideo.nativeElement.srcObject = stream)
      );
    this.callService.remoteStream$
      .pipe(filter((res) => !!res))
      .subscribe(
        (stream) => (this.remoteVideo.nativeElement.srcObject = stream)
      );
  }

  ngOnDestroy(): void {
    this.callService.destroyPeer();
  }

  public showModal(joinCall: boolean): void {
    let dialogData: DialogData = joinCall
      ? { peerId: 'null', joinCall: true }
      : { peerId: this.peerId, joinCall: false };
    const dialogRef = this.dialog.open(CallinfoDialogComponent, {
      width: '250px',
      data: dialogData,
    });

    dialogRef
      .afterClosed()
      .pipe(
        switchMap((peerId) =>
          joinCall
            ? of(this.callService.establishMediaCall(peerId))
            : of(this.callService.enableCallAnswer())
        )
      )
      .subscribe((_) => {});
  }

  public endCall() {
    this.callService.closeMediaCall();
    
    
    this.router.navigate(['prescription']);
  }
}
